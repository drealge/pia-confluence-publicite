import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-sort-container',
  templateUrl: './sort-container.component.html',
  styleUrls: ['./sort-container.component.scss']
})
export class SortContainerComponent {

  @Output() sortEmitter: EventEmitter<Sort> = new EventEmitter<Sort>();
  @Input() sorts: Sort[];

  onClickSort(newSort: string): void {
    this.sorts.forEach((sort) => {
      if (newSort === sort.orderBy) {
        sort.direction = sort.selected && !sort.direction;
        sort.selected = true;
        this.sortEmitter.emit(sort);
      } else {
        sort.selected = false;
        sort.direction = false;
      }
    });
  }
}

export class Sort {
  label: string;
  orderBy: string;
  selected: boolean;
  direction: boolean;
}
