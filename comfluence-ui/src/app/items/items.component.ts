import { AfterViewInit, Component, ElementRef, HostListener, OnDestroy, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { Device } from '../models/device/device';
import { Statistics } from '../models/device/Statistics';
import { DeviceService } from '../services/device.service';
import { Sort } from './sort-container/sort-container.component';
import { NetworkStatusService } from '../services/network-status.service';
import { Filter } from '../models/device/Filter';
import { FilterService } from '../services/filter.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SubjectsService } from 'src/app/services/subjects.service';
import { first, map } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { CoordinatesService } from 'src/app/services/coordinates.service';
import { Subscription } from 'rxjs';
import { Box4326 } from 'src/app/models/box';
import { User } from '../models/user';
import { ControlService } from '../services/control.service';
import { UserService } from '../services/user.service';
import { ControlPreparation } from '../models/control/controlPreparation';
import { OsmComponent } from '../osm/osm.component';
import { OsmMoveHelper } from '../osm/osmmove.helper';
import { DeviceMarker } from '../osm/marker/DeviceMarker';


const DEFAULT_SORT: Sort = {
  label: 'Date',
  orderBy: 'operationDate',
  selected: true,
  direction: true
};
const PARAM_FILTERID = 'filterId';
const PARAM_FILTERID_FORRELOAD = 'filterIdR';
const PARAM_ID = 'id';
const CONTROL_ID = 'idControl';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.scss']
})
export class ItemsComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChildren('drawer', { read: ElementRef }) bottomDrawer: ElementRef;
  @ViewChild('osm', { static: false }) osmComponent: OsmComponent;
  private osmMoveHelper: OsmMoveHelper;
  top = 0;
  today: number = Date.now();
  waitingForResponse = true;
  modalIsOpen = false;
  fixeDrawer = false;
  documents = false;
  showFilter = true;
  showPhoto = false;
  isFilter = false;
  lastFilterParamsSaved: any = [];
  message = 'empty';

  statistics: Statistics = <Statistics>{
    compliantDevices: 0,
    nonCompliantDevices: 0,
    todoDevices: 0
  };

  items: Device[] = [];
  selectedItem: Device = null;
  selectedFilters: Filter[] = [];
  controller: User;
  control: ControlPreparation;

  orders: Sort[] = [{
    label: 'Type',
    orderBy: 'type',
    selected: false,
    direction: false
  },
    DEFAULT_SORT
    , {
    label: 'Statut',
    orderBy: 'status',
    selected: false,
    direction: false
  }];
  private subsribList: Subscription[] = [];

  constructor(private deviceService: DeviceService,
    private router: Router,
    private route: ActivatedRoute,
    private filterService: FilterService,
    private subjectsService: SubjectsService,
    private networkStatusService: NetworkStatusService,
    private coordinatesService: CoordinatesService,
    private controlService: ControlService,
    private userService: UserService,
    private authenticationService: AuthenticationService) {
  }

  ngOnInit(): void {
    this.subsribList.push(this.coordinatesService.currentBox.subscribe(box => this.refreshItems(box)));
    this.subsribList.push(this.subjectsService.getItemModalForPhoto().subscribe(
      (bool: boolean) => {
        this.showPhoto = bool;
      }
    ));
    this.subsribList.push(this.deviceService.statisticsDispositifs(this.authenticationService.currentUserValue.departement.code)
      .subscribe((data) => {
        this.statistics = data;
      }));

  }

  ngAfterViewInit(): void {

    this.bottomDrawer['first'].nativeElement.addEventListener('scroll', this.onDrawerScroll.bind(this));
    this.osmMoveHelper = this.osmComponent.getOsmMoveHelper();
    this.route.queryParams.subscribe(params => {
      if (params[PARAM_ID]) {
        this.deviceService.get(params[PARAM_ID]).subscribe(device => {
          this.osmComponent.filterButtonControl.disable();
          this.selectedItem = device;
          this.osmMoveHelper.moveToCoordinate(device.coordinates, 18);
        });
      }
    });
  }

  @HostListener('window:scroll', ['$event'])
  onDrawerScroll(event: Event) {
    const drawer = this.bottomDrawer['first'].nativeElement.getBoundingClientRect();
    this.fixeDrawer = drawer.y < 25;
  }

  showModal(at: String): void {
    this.isFilter = at !== 'item';
    this.modalIsOpen = true;
  }

  handleFilters(filters: [Filter]) {
    this.closeModal(false);
    this.selectedFilters = filters;
    const filterIds: number[] = filters.filter(filter => !filter.needReload).map(filter => filter.id);
    const filterIdsForReload: number[] = filters.filter(filter => filter.needReload).map(filter => filter.id);
    const queryParams = {};
    if (filterIds && filterIds.length > 0) {
      queryParams[PARAM_FILTERID] = filterIds;
    }
    if (filterIdsForReload && filterIdsForReload.length > 0) {
      queryParams[PARAM_FILTERID_FORRELOAD] = filterIdsForReload;
    }
    if (queryParams[PARAM_FILTERID] || queryParams[PARAM_FILTERID_FORRELOAD]) {
      this.router.navigate(['/items'], { queryParams: queryParams });
    } else {
      this.router.navigate(['/items']);
    }
  }

  closeModal(toggle: boolean): void {
    this.modalIsOpen = toggle;
  }

  handleSort($event: Sort): void {
    this.refreshItems(this.coordinatesService.boxValue, $event);
  }

  listItems($event: MouseEvent) {
    this.selectedItem = null;
    this.documents = false;
    this.osmComponent.filterButtonControl.enable();
    this.osmComponent.resetMarkerState();
    if (this.items) {
      this.items.forEach(item => item.inBackground = false);
    }
    if (this.lastFilterParamsSaved && (this.lastFilterParamsSaved[PARAM_FILTERID] ||
      this.lastFilterParamsSaved[PARAM_FILTERID_FORRELOAD])) {
      this.router.navigate(['/items'], { queryParams: this.lastFilterParamsSaved });
    } else {
      this.router.navigate(['/items']);
    }
  }

  handleSelectedMarker(statusMarker: DeviceMarker): void {
    const device = this.items.filter((item) => item.id === statusMarker.getId()).pop();
    this.selectItem(device);
  }

  selectItem(selected: Device): void {
    if (this.selectedItem !== selected) {
      this.selectedItem = selected;
      this.selectedFilters = [];
      this.osmComponent.filterButtonControl.disable();
      this.router.navigate(['/items'], { queryParams: { id: this.selectedItem.id } });
    }
  }

  editItem(ev: MouseEvent): void {
    this.router.navigate(['/new'], { queryParams: { id: this.selectedItem.id } });
  }

  private refreshItems(box: Box4326, sort: Sort = DEFAULT_SORT) {
    this.waitingForResponse = true;
    this.route.queryParams.subscribe(params => {
      let filtersIdParam: number[] = this.convertFilterId(params[PARAM_FILTERID]) || [];
      const filterForReload: number[] = this.convertFilterId(params[PARAM_FILTERID_FORRELOAD]) || [];
      filtersIdParam = filtersIdParam.concat(filterForReload);
      const needReload = this.lastFilterParamsSaved[PARAM_FILTERID_FORRELOAD];
      const idControl = params[CONTROL_ID];
      const selectedIdStr = params[PARAM_ID];
      let selectedId: number;
      if (selectedIdStr) {
        selectedId = parseInt(selectedIdStr, 10);
      }
      if (idControl) {
        this.lastFilterParamsSaved = params;
        this.controlService.get(idControl).pipe(first()).subscribe(controlData => {
          this.control = controlData;
          this.userService.findByMail(this.control.controllerMail).pipe(first()).subscribe(user => this.controller = user);
        });
        this.deviceService.findAllByControlId(idControl)
          .pipe(map(devices => devices.sort((a, b) => this.sortBy(sort, a, b))))
          .subscribe((items: Device[]) => {
            if (selectedId && items) {
              items.forEach(item => {
                if (item.id !== selectedId) {
                  item.inBackground = true;
                }
              });
            }
            if (filtersIdParam) {
              this.lastFilterParamsSaved = params;
              this.onApplyFilter(items, filtersIdParam);
            } else {
              this.items = items;
              this.selectedFilters = [];
              this.waitingForResponse = false;
            }
          });
      } else {
        this.deviceService.findAllFromBox(box, filterForReload, needReload)
          .pipe(map(devices => devices.sort((a, b) => this.sortBy(sort, a, b))))
          .subscribe((items: Device[]) => {
            if (selectedId && items) {
              items.forEach(item => {
                if (item.id !== selectedId) {
                  item.inBackground = true;
                }
              });
            }
            if (filtersIdParam) {
              this.lastFilterParamsSaved = params;
              this.onApplyFilter(items, filtersIdParam);
            } else {
              this.items = items;
              this.selectedFilters = [];
              this.waitingForResponse = false;
            }
          });
      }
      if (!params[PARAM_ID] && !filtersIdParam) {
        this.lastFilterParamsSaved = null;
      }
    }, (err: Error) => {
      console.error(err);
      this.waitingForResponse = false;
      this.message = 'error';
    });
  }

  handleOnClickDocuments(ev: MouseEvent) {
    this.toggleDocuments();
  }

  private toggleDocuments() {
    this.fixeDrawer = false;
    this.documents = !this.documents;
  }

  onShowFilterPopIn() {
    this.isFilter = true;
    this.showModal('filter');
  }
  private convertFilterId(idToConvert: String | String[] | number[]): number[] | undefined {
    if (!idToConvert) {
      return undefined;
    }
    let filtersId: number[] = [];
    if (idToConvert instanceof Array) {
        filtersId = (<String[]>idToConvert).map(value => Number(value));
    } else {
      filtersId = [Number(idToConvert)];
    }

    return filtersId;
  }
  onApplyFilter(items: Device[], filtersIdParam: String | String[] | number[]) {
    let filtersId: number[] = [];
    if (filtersIdParam) {
      filtersId = this.convertFilterId(filtersIdParam);
    }

    if (filtersId && filtersId.length > 0) {
      this.filterService.findByIds(filtersId).subscribe(
        (res: Filter[]) => {
          this.selectedFilters = res;
          this.items = items.filter(
            item => item.tags && this.selectedFilters
              .every(filter => filter.needReload || item.tags.map(tag => tag.id).includes(filter.id)));
          this.waitingForResponse = false;
        });
    } else {
      this.items = items;
      this.waitingForResponse = false;
    }
  }


  handleCloseProcedure(item: Device) {
    this.selectedItem = item;
  }

  get networkOffLine(): boolean {
    return this.networkStatusService.isNetworkOffLine();
  }

  private sortBy(sort: Sort, a: Device, b: Device): number {
    const aValue = a[sort.orderBy];
    const bValue = b[sort.orderBy];
    const direction = sort.direction ? 1 : -1;
    let sortMethod = (stringA, stringB) => stringA < stringB ? direction : -direction;
    if (aValue instanceof Date) {
      sortMethod = (dateA, dateB) => direction * (dateA.getTime() - dateB.getTime());
    }
    return sortMethod(aValue, bValue);
  }

  ngOnDestroy() {
    this.subsribList.forEach(subscription => subscription.unsubscribe());
  }
}
