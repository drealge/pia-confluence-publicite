import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { DocumentService } from 'src/app/shared/services/document.service';
import { Document } from 'src/app/models/device/document';

@Component({
  selector: 'app-upload-odt',
  templateUrl: './upload-odt.component.html',
  styleUrls: ['./upload-odt.component.scss']
})
export class UploadOdtComponent implements OnInit {
  files: File[] = [];
  message: String;
  acceptTypeFile = ['.odt', '.pdf'];
  isWaitingForResponse = false;

  @Output() cancel = new EventEmitter<boolean>();
  @Output() editedDoc = new EventEmitter<Document>();
  @Input() idDevice;
  @Input() idPreview;

  constructor(private documentService: DocumentService) {
  }

  ngOnInit() {
  }


  public onDragOver(evt: DragEvent) {
    evt.preventDefault();
    evt.stopPropagation();
  }


  public onDragEnter(evt: DragEvent) {
    (<HTMLDivElement>evt.target).classList.add('dragging');
    evt.preventDefault();
    evt.stopPropagation();
  }

  public onDragLeave(evt: DragEvent) {
    (<HTMLDivElement>evt.target).classList.remove('dragging');
    evt.preventDefault();
    evt.stopPropagation();
  }

  public onDrop(evt: DragEvent) {
    evt.preventDefault();
    evt.stopPropagation();

    const newFiles = evt.dataTransfer.files;
    if (newFiles.length > 0) {
      if (this.verifyType(newFiles[0])) {
        this.files = [newFiles[0]];
      } else {
        this.showErrorMessage(true);
      }
    }
  }

  uploadFile(event): void {
    const element: File = event[0];
    if (this.verifyType(element)) {
      this.files = [element];
    } else {
      this.showErrorMessage(true);
    }
  }

  private verifyType(el: File): boolean {
    let isAcceptType = false;
    if (el && el.name) {
      const fileType = `.${el.name.split('.').pop()}`;
      isAcceptType = this.acceptTypeFile.includes(fileType);
    }
    return isAcceptType;
  }

  deleteAttachment() {
    this.files = [];
  }

  onCloseModal(): void {
    this.cancel.emit();
  }

  showErrorMessage(ishow: boolean): void {
    if (ishow) {
      this.message = 'Navré, nous ne pouvons pas utiliser ce type de fichier.';
    } else {
      this.message = null;
    }
  }

  onPostNewOdt() {
    this.isWaitingForResponse = true;
    this.documentService.uploadOdt(this.files[0], this.idDevice, this.idPreview).subscribe({
      next: res => this.handleResponse(res),
      error: err => console.error(err)
    });
  }

  handleResponse(document: Document): void {
    this.editedDoc.emit(document);
    this.cancel.emit();
  }

}
