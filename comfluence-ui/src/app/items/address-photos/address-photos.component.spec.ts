import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddressPhotosComponent } from './address-photos.component';

describe('AddressPhotosComponent', () => {
  let component: AddressPhotosComponent;
  let fixture: ComponentFixture<AddressPhotosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddressPhotosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddressPhotosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
