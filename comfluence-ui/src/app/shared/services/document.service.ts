import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Inject, Injectable, LOCALE_ID } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseService } from './base.service';
import { Document } from '../../models/device/document';
import { map } from 'rxjs/operators';
import { formatDate } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class DocumentService extends BaseService<Document> {

  constructor(private httpClient: HttpClient,
    @Inject(LOCALE_ID) public locale: string) {
    super('document', httpClient);
  }

  public findAll(): Observable<Document[]> {
    throw new Error('Not implemented');
  }

  public urlPreviewDocument(id: number) {
    return `${this.baseUrl}/preview/${id}`;
  }

  public urlDownloadDocument(id) {
    return `${this.baseUrl}/download/${id}`;
  }

  public get(id: number) {
    let params = new HttpParams();
    params = params.set('contentType', 'application/pdf');
    return this.httpClient.get(`${this.baseUrl}/preview/${id}`, { responseType: 'blob', params: params });
  }

  public getForDownload(id: number) {
    return this.httpClient.get(`${this.baseUrl}/download/${id}`, { observe: 'response', responseType: 'blob' });
  }

  public save(document: Document): Observable<Document> {
    const form: FormData = new FormData();
    form.append('idDevice', document.idDevice.toString());
    form.append('comment', document.comment);
    if (document.file) {
      form.append('file', document.file);
    }
    if (document.changeType) {
      form.append('changeType', document.changeType);
    }
    if (document.procedure) {
      form.append('procedure.id', document.procedure.id.toString());
    }
    if (document.delay) {
      form.append('delay', document.delay.toString());
    }
    form.append('addedDate', new Date().toISOString().replace('Z', '+00:00'));
    return this.httpClient.post<Document>(`${this.baseUrl}`, form);
  }

  public findAllByDevice(idDispositif: number): Observable<Document[]> {
    return this.httpClient.get<Document[]>(`${this.baseUrl}/byDevice/${idDispositif}`).pipe(map(
      documents => {
        documents.forEach(document => document.addedDate = new Date(document.addedDate));
        return documents;
      }
    ));
  }

  public closeProcedure(idDispositif: number, idDoc: number, closeDate?: Date, sansSuite?: Boolean): Observable<Boolean> {
    let params = new HttpParams();
    if (closeDate) {
      params = params.set('closeDate', closeDate.toDateString());
      params = params.set('sansSuite', sansSuite.toString());
    }
    return this.httpClient.put<Boolean>(`${this.baseUrl}/${idDispositif}/${idDoc}/close`, null, { params: params });
  }

  public uploadOdt(uploadedOdt: File, idDevice: number, idDocument: number): Observable<Document> {
    const form: FormData = new FormData();
    form.append('file', uploadedOdt);
    return this.httpClient.post<Document>(`${this.baseUrl}/${idDevice}/${idDocument}`, form);
  }

  public update(id: number, value: Document): Observable<Document> {
    return this.httpClient.patch<Document>(`${this.baseUrl}/${id}`, value);
  }

  public licorneExport(depCode: number, dateMin: Date, dateMax: Date): Observable<HttpResponse<Blob>> {
    const httpParams = new HttpParams()
      .append('dateMin', formatDate(dateMin, 'yyyy-MM-dd', this.locale))
      .append('dateMax', formatDate(dateMax, 'yyyy-MM-dd', this.locale));
    return this.httpClient.get(`${this.baseUrl}/licorne/${depCode}`, { params: httpParams, observe: 'response', responseType: 'blob' });
  }
}
