import { Injectable } from '@angular/core';
import { ReplayResultsService } from './replay-results.service';
import { Filter } from '../../models/device/Filter';

@Injectable({
  providedIn: 'root'
})
export class ReplayFiltersService extends ReplayResultsService<Filter> {
  constructor() {
    super(1);
  }
}
