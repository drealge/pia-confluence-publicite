import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { HttpClient } from '@angular/common/http';
import { Infrigement } from '../../models/device/properties/infrigement';

@Injectable({
  providedIn: 'root'
})
export class InfrigementService extends BaseService<Infrigement> {

  constructor(private httpClient: HttpClient) {
    super('infrigement', httpClient);
  }
}
