import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'enumToString'
})
export class EnumToStringPipe implements PipeTransform {

  transform(value: String): string {
    switch (value) {
      case 'AUTHORIZATION':
        return 'Autorisation préalable';
      case 'DECLARATION':
        return 'Déclaration';
      case 'OUTSTATE':
        return 'Contrôle inopiné';
      case 'AD':
        return 'Publicité';
      case 'SIGNBOARD':
        return 'Enseigne';
      case 'PRESIGNBOARD':
        return 'Pré-enseigne';
      case 'SMALL':
        return '< 4m²';
      case 'MEDIUM':
        return '4m² < x <8m²';
      case 'LARGE':
        return '8m² < x 12m²';
      case 'XLARGE':
        return '>12m²';
      case 'BRIGHT':
        return 'Lumineux';
      case 'FLOOR':
        return 'Scellé au sol';
      case 'NUMERIC':
        return 'Numérique';
      case 'ALL':
        return 'Ensemble';
      case 'NEAR':
        return 'Proche';
      case 'BUTEAU':
        return 'Buteau';
      case 'DOUBLE_FACE':
        return 'Double Face';
      case 'REEXAMINATION':
        return 'Contre-visite';
      case 'MODIFIED':
        return 'Modifié';
      case 'ADDED':
        return 'Ajouté';
      case 'SUSPENDED_INFRIGEMENT':
        return 'Levée';
      default:
        return '';
    }
  }
}
