import { Pipe, PipeTransform } from '@angular/core';
import { Status } from '../../models/device/device';

@Pipe({
  name: 'statusEnumToCss'
})
export class StatusEnumToCssPipe implements PipeTransform {
  transform(value: Status) {
    switch (value) {
      case Status.COMPLIANT:
        return 'comply';
        break;
      case Status.NONCOMPLIANT:
        return 'non-compliant';
        break;
      case Status.TODO:
        return 'undefined';
        break;
      case Status.NOTSELECTED:
        return 'not-selected';
        break;
      default:
        return 'String not found for ' + value;
        break;
    }
  }
}
