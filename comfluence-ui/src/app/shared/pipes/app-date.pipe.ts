import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'appDate'
})
export class AppDatePipe implements PipeTransform {

  private static padDateTwoDigits(date: number): string {
    return date.toString().padStart(2, '0');
  }

  transform(date: number | string | Date): any {
    if (date) {
      let pDate;
      if (date instanceof Date) {
        pDate = date;
      } else {
        pDate = new Date(date);
      }
      return AppDatePipe.padDateTwoDigits(pDate.getDate()) + '.'
        + AppDatePipe.padDateTwoDigits(pDate.getMonth() + 1)
        + '.' + pDate.getFullYear();
    } else {
      return '--.--.--';
    }
  }
}
