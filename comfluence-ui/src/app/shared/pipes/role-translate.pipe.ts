import { Pipe, PipeTransform } from '@angular/core';
import { Role } from 'src/app/models/user';

@Pipe({
  name: 'translateRole'
})
export class RoleTranslatePipe implements PipeTransform {
  transform(value: Role) {
    switch (value.name) {
      case 'USER':
        return 'Utilisateur';
      case 'ADMIN':
        return 'Administrateur';
      default:
        return '??? ' + value + ' ???';
    }
  }
}
