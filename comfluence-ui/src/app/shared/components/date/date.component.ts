import { Component, Input } from '@angular/core';
import { Status, Type } from '../../../models/device/device';

@Component({
  selector: 'app-date',
  templateUrl: './date.component.html',
  styleUrls: ['./date.component.scss']
})
export class DateComponent {
  @Input() displayedDate: Date;
  @Input() status: Status;
  @Input() type: Type;
}
