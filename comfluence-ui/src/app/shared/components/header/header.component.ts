import { Component, EventEmitter, Input, OnDestroy, Output } from '@angular/core';
import { Router } from '@angular/router';
import { StateService } from 'src/app/services/state.service';
import { RetryRequestHelper } from 'src/app/helpers/request/requestcache.helper';
import { Subscription } from 'rxjs';
import { User } from '../../../models/user';
import { NetworkStatusService } from '../../../services/network-status.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnDestroy {
  @Input() navigation = true;
  @Input() currentUser: User;
  @Output() logoutUser = new EventEmitter<boolean>();
  userMenuOpened = false;
  unsyncCount = 0;
  countSubscription: Subscription;
  networkOffLine = false;

  constructor(private router: Router,
              private networkStatusService: NetworkStatusService,
              private stateService: StateService,
              private retryRequestHelper: RetryRequestHelper) {
    this.countSubscription = this.stateService.unsyncValueObs.subscribe(unsyncCount => this.unsyncCount = unsyncCount);
    this.networkStatusService.onLine.subscribe(online => {
      if (this.networkOffLine !== !online) {
        this.networkOffLine = !online;
      }
    });
  }

  backToDashboard(): void {
    this.router.navigate(['/']);
  }

  logout() {
    this.logoutUser.emit(true);
  }

  toggleUserMenuOpenState() {
    this.userMenuOpened = !this.userMenuOpened;
  }

  tryToResync() {
    this.retryRequestHelper.retryRequests();
  }

  ngOnDestroy(): void {
    this.countSubscription.unsubscribe();
  }
}
