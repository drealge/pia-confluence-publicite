import { Component, AfterContentInit, OnDestroy, ContentChild, Input, ElementRef, Renderer2, HostListener, Inject } from '@angular/core';
import { ActionMenuOriginDirective } from './action-menu-origin';
import { ActionMenuDirective } from './action-menu';
import { Subscription } from 'rxjs';
import { DOCUMENT } from '@angular/common';

/** Inspired by https://netbasal.com/create-powerful-action-menu-with-angular-and-popper-eb047f789f2c */
@Component({
    selector: 'app-action-menu',
    template: `
      <ng-content></ng-content>
    `,
    styleUrls: ['./action-menu.component.scss']
})
export class ActionMenuComponent implements AfterContentInit, OnDestroy {

    @ContentChild(ActionMenuOriginDirective, { static: false }) origin: ActionMenuOriginDirective;
    @ContentChild(ActionMenuDirective, { static: false }) dropdown: ActionMenuDirective;

    private _open = false;
    private _originSub: Subscription;

    constructor(private host: ElementRef,
        private renderer: Renderer2,
        @Inject(DOCUMENT) private readonly document: Document) {

    }

    @HostListener('document:click', ['$event.target'])
    click(target) {
        if (!(this.host.nativeElement as HTMLElement).contains(target)) {
            this.close();
        }
    }


    /**
     * Subscribe to the origin click event
     */
    ngAfterContentInit() {
        this._originSub = this.origin.click.subscribe(_ => {
            this._open = !this._open;
            if (this._open) {
                this.open();
            } else {
                this.close();
            }
        });
    }

    /**
     * Append the dropdown to body and init popper
     */
    open() {
        this._open = true;
        this._toggleDropdown(true);
    }

    /**
     * Destroy popper and hide the dropdown
     */
    close() {
        this._open = false;
        this._toggleDropdown(false);
    }

    private _toggleDropdown(show = true) {
        const display = show ? 'block' : 'none';
        this.renderer.setStyle(this.dropdown.element, 'display', display);
    }

    /**
     * Cleaning
     */
    ngOnDestroy() {
        if (this._originSub) {
            this._originSub.unsubscribe();
        }
    }
}
