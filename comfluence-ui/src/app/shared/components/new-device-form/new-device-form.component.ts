import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Guidance, Type } from '../../../models/device/device';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

const DEFAULT_GUIDANCE_LIST = [Guidance.DECLARATION, Guidance.AUTHORIZATION, Guidance.OUTSTATE];

@Component({
  selector: 'app-new-device-modal',
  templateUrl: './new-device-form.component.html',
  styleUrls: ['../modal/modal.component.scss']
})
export class NewDeviceFormComponent implements OnInit {
  @Output() cancel = new EventEmitter<boolean>();

  types: Type[] = [Type.AD, Type.PRESIGNBOARD, Type.SIGNBOARD];
  guidances: Guidance[] = DEFAULT_GUIDANCE_LIST;
  deviceTypeForm = new FormGroup({
    type: new FormControl(this.types[0].toString()),
    guidance: new FormControl(this.guidances[0].toString())
  });

  constructor(private router: Router) {
  }

  ngOnInit(): void {
    this.deviceTypeForm.valueChanges.subscribe((values) => {
      if (values.type === Type.SIGNBOARD) {
        this.guidances = [Guidance.AUTHORIZATION, Guidance.OUTSTATE];
      } else {
        this.guidances = DEFAULT_GUIDANCE_LIST;
      }
    });
  }

  cancelClick(): void {
    this.cancel.emit(false);
  }


  submitTypeForm(): void {
    this.router.navigate(['/new'], { queryParams: this.deviceTypeForm.value });
  }
}
