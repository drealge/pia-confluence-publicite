import { Component, EventEmitter, HostBinding, HostListener, Input, OnInit, Output } from '@angular/core';
import { transition, trigger } from '@angular/animations';

export enum COLOR_SET {
  INFORMATION = 'information',
  WARNING = 'warning'
}

@Component({
  selector: 'app-confirm-modal',
  templateUrl: './confirm-modal.component.html',
  styleUrls: ['./confirm-modal.component.scss'],
  animations: [
    trigger('modal', [
      transition('void => *', []),
      transition('* => void', [])
    ])
  ]
})
export class ConfirmModalComponent implements OnInit {
  private static readonly DEFAULT_CANCEL_LABEL = 'Annuler';
  private static readonly DEFAULT_CONFIRM_LABEL = 'Valider';
  @Output() cancel = new EventEmitter();
  @Output() accept = new EventEmitter();
  @Output() isOpenChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Input() isOpen: boolean;
  @Input() cancelLabel = ConfirmModalComponent.DEFAULT_CANCEL_LABEL;
  @Input() confirmLabel = ConfirmModalComponent.DEFAULT_CONFIRM_LABEL;
  @Input() autoClose = true;
  @Input() confirmButton = true;

  // tslint:disable-next-line:no-input-rename
  @HostBinding('class') @Input('class') classList = '';

  @HostListener('document:keydown.escape', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    if (this.isOpen && this.autoClose) {
      this.handleClose();
    }
  }

  ngOnInit(): void {
    if (!Object.keys(COLOR_SET).some(theme => this.classList.split(/\s/).includes(theme))) {
      this.classList += ' default ';
    }
  }

  handleAccept(event: MouseEvent) {
    event.stopPropagation();
    if (this.autoClose) {
      this.closeModal();
    }
    this.accept.emit();
  }

  handleCancel(event: MouseEvent) {
    event.stopPropagation();
    if (this.autoClose) {
      this.closeModal();
    }
    this.cancel.emit();
  }

  handleClose(event?: MouseEvent) {
    if (event) {
      event.stopPropagation();
    }
    this.closeModal();
  }

  private closeModal() {
    this.isOpen = false;
    this.isOpenChange.emit(this.isOpen);
  }
}
