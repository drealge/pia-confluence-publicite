import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output } from '@angular/core';
import { Context, Location } from 'src/app/models/device/properties/context';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { CoordinatesService } from 'src/app/services/coordinates.service';
import { LayerService } from 'src/app/services/layer.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { first } from 'rxjs/operators';
import proj4 from 'proj4';

@Component({
  selector: 'app-context',
  templateUrl: './context.component.html',
  styleUrls: ['./context.component.scss']
})
export class ContextComponent implements OnChanges, OnInit, OnDestroy {

  @Output() contextChange = new EventEmitter<Context>();
  @Input() context: Context;
  @Input() readOnly = false;
  loading = false;
  private subscriptionList = [];

  contextForm: FormGroup;
  radiosDef = [
    { id: 'yes-agglo', label: 'Oui', valueSelected: Location.AGGLOMERATION },
    { id: 'no-agglo', label: 'Non', valueSelected: Location.COUNTRYSIDE },
    { id: 'undefined-agglo', label: 'Indéfini', valueSelected: Location.UNDEFINED_LOCATION }
  ];

  constructor(private formBuilder: FormBuilder, private coordinatesService: CoordinatesService,
    private layerService: LayerService, private authenticateService: AuthenticationService) {
    this.contextForm = this.formBuilder.group({
      id: this.formBuilder.control(null),
      town: this.formBuilder.control(null, Validators.required),
      population: this.formBuilder.control(null, Validators.required),
      unit: this.formBuilder.control(null, Validators.required),
      constraints: this.formBuilder.control(null),
      location: new FormControl(Location.AGGLOMERATION)
    });
    this.subscriptionList.push(this.coordinatesService.coordinates.subscribe(coordinate => {
      this.loading = true;
      const epsg2154Coordinate = proj4('EPSG:4326', 'EPSG:2154', [coordinate.longitude, coordinate.latitude]);
      layerService.searchRestrictions(this.authenticateService.currentUserValue.departement.code,
        epsg2154Coordinate[0], epsg2154Coordinate[1]).pipe(first()).subscribe(restrictionList => {
          this.loading = false;
          this.context.constraints = restrictionList;
        }, error => {
          this.loading = false;
        });
    }));
  }

  ngOnInit(): void {
    this.subscriptionList.push(this.contextForm.valueChanges.subscribe(values => {
      values.constraints = [];
      this.context = values;
      this.contextChange.emit(values);
    }));
  }

  ngOnChanges(): void {
    if (this.context) {
      this.contextForm.patchValue(this.context, { emitEvent: false });
    }
  }

  ngOnDestroy(): void {
    this.subscriptionList.forEach(subscription => subscription.unsubscribe());
  }

  getRadioDefForContextLocation() {
    return this.radiosDef.find(radio => radio.valueSelected === this.context.location);
  }
}
