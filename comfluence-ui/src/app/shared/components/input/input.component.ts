import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-input',
  styleUrls: ['./input.component.scss'],
  templateUrl: './input.component.html',
})
export class InputComponent {
  @Input() label: string;
  @Input() for: string;
  @Input() readOnly = false;
  @Input() value: any;
  @Input() required = false;
}
