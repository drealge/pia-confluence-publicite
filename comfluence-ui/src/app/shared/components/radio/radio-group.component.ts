import { Component, forwardRef, Input, OnDestroy, OnInit } from '@angular/core';
import { ControlValueAccessor, FormBuilder, FormControl, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Subscription } from 'rxjs';

export class Radio {
  id?: string;
  label: string;
  valueSelected: any;
  click?: (value: any) => any;
}


@Component({
  selector: 'app-radio-group',
  templateUrl: './radio-group.component.html',
  styleUrls: ['./radio-group.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => RadioGroupComponent),
      multi: true
    }
  ]
})
export class RadioGroupComponent implements ControlValueAccessor, OnInit, OnDestroy {

  static radioGroupCount = 0;
  static radioCount = 0;

  @Input() radios: Radio[];
  @Input() columns = 1;

  disabled = false;
  radioGroupName = 'radio-group-';
  radioControl: FormControl;
  private subscription: Subscription;
  // permit the initialisation of the radio group first value
  private isInit = false;

  onChange = (data: any) => {};
  onTouched = () => {};

  constructor(private formBuilder: FormBuilder) {
    this.radioControl = this.formBuilder.control(null);
    this.radioGroupName += RadioGroupComponent.radioGroupCount++;
    this.subscription = this.radioControl.valueChanges.subscribe(value => {
      const selectedRadio = this.radios.find(radio => radio.valueSelected.toString() === value);
      this.onChange(selectedRadio ? selectedRadio.valueSelected : value);
    });
  }

  ngOnInit(): void {
    this.radios.forEach(radio => {
      if (!radio.id) {
        radio.id = `radio-${RadioGroupComponent.radioCount++}`;
      }
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  registerOnChange(fn: any) {
    this.onChange = fn;
  }

  registerOnTouched(fn: any) {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  writeValue(val: any): void {
    if (val !== null && val !== undefined && !this.isInit) {
      this.radioControl.patchValue(val.toString(), {emitEvent: false});
      this.isInit = true;
    }
  }

  radioClick(radio: Radio) {
    if (radio.click) {
      radio.click(radio.valueSelected);
    }
  }
}
