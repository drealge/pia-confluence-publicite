import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { NewDeviceComponent } from './new-device/new-device.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { PhotoComponent } from './components/photo/photo.component';
import { AddressComponent } from './components/address/address.component';
import { PhotosBlockComponent } from './components/photos-block/photos-block.component';
import { InfrigementComponent } from './components/infrigement/infrigement.component';
import { NearbyDevicesComponent } from './components/nearby-devices/nearby-devices.component';
import { EditHeaderComponent } from './components/edit-header/edit-header.component';
import { CameraComponent } from './components/camera/camera.component';
import { NewPhotoComponent } from './components/new-photo/new-photo.component';

const routes: Routes = [
  {path: '', component: NewDeviceComponent},
];


@NgModule({
  declarations: [
    NewDeviceComponent,
    NewPhotoComponent,
    CameraComponent,
    AddressComponent,
    PhotosBlockComponent,
    PhotoComponent,
    InfrigementComponent,
    NearbyDevicesComponent,
    EditHeaderComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class NewModule {
}
