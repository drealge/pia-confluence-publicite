import { Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';


@Component({
  selector: 'app-camera',
  templateUrl: './camera.component.html',
  styleUrls: ['./camera.component.scss']
})
export class CameraComponent implements OnDestroy, OnInit {
  @Output() isOpen = new EventEmitter<boolean>();
  @Output() fileEmitter = new EventEmitter<string>();
  @Input() photoSrc: String;
  @Input() photoId: number;

  @ViewChild('video', {static: false})
  public video: ElementRef;

  @ViewChild('canvas', {static: false})
  public canvas: ElementRef;
  @ViewChild('zoomInput', {static: false})
  public zoomInput: ElementRef;

  public file: string;

  private localStream: MediaStream;

  isTaken = false;
  windowWidth: number;
  windowHeight: number;

  displayZoom = false;
  zoomMin = 0;
  zoomMax = 0;
  zoomStep = 1;
  private cameraLoaded = false;

  public constructor() {
    this.file = null;
  }

  ngOnInit(): void {
    this.prepareCamera().then(cameraLoaded => this.cameraLoaded = cameraLoaded);
    this.windowWidth = window.innerWidth;
    this.windowHeight = window.innerHeight;
  }

  public ngOnDestroy(): void {
    this.closeCamera();
  }

  async prepareCamera(): Promise<boolean> {
    if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
      try {
        this.localStream = await navigator.mediaDevices.getUserMedia({video: {facingMode: 'environment'}});
      } catch (e) {
        console.warn(e);
        return false;
      }
      this.video.nativeElement.srcObject = this.localStream;
      // Timeout needed in Chrome, see https://crbug.com/711524
      await this.sleep(1000);
      const track = this.localStream.getVideoTracks()[0];
      const capabilities = <any>track.getCapabilities();

      // Check whether zoom is supported or not.
      if (!('zoom' in capabilities)) {
        console.log('Pas de zoom disponible');
      } else {
        this.zoomMin = capabilities.zoom.min;
        this.zoomMax = capabilities.zoom.max;
        this.zoomStep = capabilities.zoom.step;
        this.zoomInput.nativeElement.oninput = function (event) {
          track.applyConstraints(<any>{advanced: [{zoom: event.target.value}]});
        };
        this.displayZoom = true;
      }
      this.video.nativeElement.play();
    }
    return true;
  }

  private sleep(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  closeCamera() {
    if (this.video) {
      this.video.nativeElement.src = '';
      this.video.nativeElement.pause();
    }
    if (this.localStream) {
      this.localStream.getTracks()[0].stop();
    }
  }

  public capture() {
    const canvas = this.canvas.nativeElement;
    const video = this.video.nativeElement;
    const hRatio = canvas.width / video.videoWidth;
    const vRatio = canvas.height / video.videoHeight;
    const ratio = Math.min(hRatio, vRatio);
    const centerShift_x = (canvas.width - video.videoWidth * ratio) / 2;
    const centerShift_y = (canvas.height - video.videoHeight * ratio) / 2;
    canvas.getContext('2d').drawImage(video, 0, 0,
      video.videoWidth, video.videoHeight,
      centerShift_x, centerShift_y, video.videoWidth * ratio, video.videoHeight * ratio);
    this.file = this.canvas.nativeElement.toDataURL('image/png');
    this.isTaken = true;
  }

  public newPhoto() {
    this.deleteSavedPhoto();
    this.isTaken = false;
    this.prepareCamera().then(cameraLoaded => this.cameraLoaded = cameraLoaded);
  }

  public deleteSavedPhoto(): void {
    this.file = null;
    this.photoSrc = null;
    this.fileEmitter.emit(this.file);
  }

  public save() {
    this.fileEmitter.emit(this.file);
    this.isOpen.emit(false);
  }
}
