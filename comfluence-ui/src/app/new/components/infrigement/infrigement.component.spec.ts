import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Infrigement } from './infrigement.component';

describe('InfrigementComponentComponent', () => {
  let component: Infrigement;
  let fixture: ComponentFixture<Infrigement>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Infrigement ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Infrigement);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
