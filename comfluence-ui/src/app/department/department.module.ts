import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DepartmentComponent } from './department.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { DatatableModule } from '../datatable/datatable.module';
import { ReactiveFormsModule } from '@angular/forms';

const routes: Routes = [
  {path: '', component: DepartmentComponent},
];


@NgModule({
  declarations: [
    DepartmentComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    SharedModule,
    CommonModule,
    DatatableModule,
    ReactiveFormsModule
  ]
})
export class DepartmentModule {
}
