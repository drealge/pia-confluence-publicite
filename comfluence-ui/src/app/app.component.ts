import { Component, OnInit } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';
import { NavigationStart, Router, Event } from '@angular/router';
import { AuthenticationService } from './services/authentication.service';
import { User } from './models/user';
import proj4 from 'proj4';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  hideHeader = true;
  pathWithOutHeader = '/new';
  currentUser: User;

  constructor(private swUpdate: SwUpdate, private router: Router,
    private authenticationService: AuthenticationService) {
    proj4.defs('EPSG:2154', '+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000' +
      ' +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs');
    proj4.defs('EPSG:4326', '+proj=longlat +datum=WGS84 +no_defs');
    this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user;
      this.router.events.subscribe(event => {
        if (event instanceof NavigationStart) {
          this.hideHeader = !user || event.url.startsWith(this.pathWithOutHeader);
        }
      });
    });
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }

  ngOnInit(): void {
    if (this.swUpdate.isEnabled) {
      this.swUpdate.available.subscribe(() => {
        if (confirm('New version available. Load new version ?')) {
          window.location.reload();
        }
      });
    }
  }
}

