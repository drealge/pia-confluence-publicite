import * as L from 'leaflet';
import { Status } from 'src/app/models/device/device';
import { DeviceMarker } from '../marker/DeviceMarker';
import { deviceClusterIcon } from '../icon/DeviceIcon';

class DeviceMarkerClusterGroup extends L.MarkerClusterGroup {
  constructor(options?: L.MarkerClusterGroupOptions) {
    super();
    if (!options || !options.iconCreateFunction) {
      L.Util.setOptions(this, {
        iconCreateFunction: this.iconCreateFunction.bind(this)
      });
    }
    L.Util.setOptions(this, options);
  }

  private iconCreateFunction(cluster: L.MarkerCluster) {
    const deviceMarkers = (cluster.getAllChildMarkers() as DeviceMarker[]);

    const datas: DeviceByStatus[] = [];
    datas.push(this.getSubCluster(deviceMarkers, Status.NONCOMPLIANT));
    datas.push(this.getSubCluster(deviceMarkers, Status.TODO));
    datas.push(this.getSubCluster(deviceMarkers, Status.COMPLIANT));

    const max: DeviceByStatus = datas.reduce((prev, current) => (prev.size >= current.size) ? prev : current);
    return deviceClusterIcon(max.status, cluster.getChildCount());
  }

  private getSubCluster(markers: DeviceMarker[], status: Status): DeviceByStatus {
    return {
      size: markers.filter((marker) => marker.getStatus() === status).length,
      status: status
    };
  }
}

interface DeviceByStatus {
  size: number;
  status: Status;
}

export function deviceMarkerClusterGroup(options?: L.MarkerClusterGroupOptions): DeviceMarkerClusterGroup {
  return new DeviceMarkerClusterGroup(options);
}
