import * as L from 'leaflet';
import { ControlOptions } from 'leaflet';

export interface OnFilterHandler {
  onFilterClicked();
}

export class FilterButton extends L.Control {
  private map: L.Map;
  private onFilterHandler: OnFilterHandler;
  private _enabled;

  constructor(map: L.Map, callback: OnFilterHandler, options?: ControlOptions) {
    super(options);
    this.map = map;
    this.onFilterHandler = callback;
  }

  onAdd(map: L.Map): HTMLElement {
    const controlButton = L.DomUtil.create('div', 'leaflet-bar leaflet-control leaflet-control-custom');
    const icon = <HTMLAnchorElement>L.DomUtil.create('a', 'btn-filter',  controlButton);

    L.DomEvent.on(controlButton, 'dblclick', L.DomEvent.stopPropagation);
    L.DomEvent.on(controlButton, 'click', this.toggleFilter, this);

    return controlButton;
  }

  onRemove(map: L.Map): void {
    L.DomEvent.off(this.getContainer(), 'dblclick', L.DomEvent.stopPropagation);
    L.DomEvent.off(this.getContainer(), 'click', this.toggleFilter, this);
  }

  private toggleFilter(event: Event) {
    event.preventDefault();
    event.stopPropagation();
    if (this.onFilterHandler) {
      this.onFilterHandler.onFilterClicked();
    }
  }

  public disable() {
    if (this._enabled && this.getContainer()) {
      this.getContainer().style.display = 'none';
      this.toggleEnabled();
    }
  }

  public enable() {
    if (!this._enabled) {
      if (!this.getContainer()) {
        this.map.addControl(this);
      }
      this.getContainer().style.display = null;
      this.toggleEnabled();
    }
  }

  private toggleEnabled() {
    this._enabled = !this._enabled;
  }
}

export function filterButton(map: L.Map, callback: OnFilterHandler, options?: ControlOptions): FilterButton {
  return new FilterButton(map, callback, options);
}
