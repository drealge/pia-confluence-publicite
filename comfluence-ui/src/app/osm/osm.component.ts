import {
    AfterViewInit,
    Component,
    ElementRef,
    EventEmitter,
    HostListener,
    Input,
    OnChanges,
    OnInit,
    Output,
    SimpleChanges,
    ViewChild
} from '@angular/core';

import { DeviceMarker, deviceMarker } from './marker/DeviceMarker';
import { Device, Status } from 'src/app/models/device/device';
import { deviceIcon } from './icon/DeviceIcon';
import * as L from 'leaflet';
import 'proj4leaflet';
import 'leaflet.markercluster';
import { LExtended } from 'geoportal-extensions-leaflet';
import { Coordinates } from 'src/app/models/device/properties/coordinates';
import { Filter } from 'src/app/models/device/Filter';
import { SubjectsService } from 'src/app/services/subjects.service';
import { myPositionButton } from './button/MyPositionButton';
import { FilterButton, filterButton, OnFilterHandler } from './button/FilterButton';
import { measurementsButton } from './button/MeasurementsButton';
import { deviceMarkerClusterGroup } from './cluster/DeviceMarkerClusterGroup';
import { CoordinatesService } from 'src/app/services/coordinates.service';
import { lockMapButton } from './button/LockMapButton';
import { LayerDefinition, LayerService } from 'src/app/services/layer.service';
import { FeatureCollection, Geometry } from 'geojson';
import { DrawButton, drawButton } from './button/DrawButton';
import { ControlPreparation } from 'src/app/models/control/controlPreparation';
import { GeometryService } from 'src/app/services/geometry.service';
import { OsmMoveHelper } from './osmmove.helper';
import { AuthenticationService } from 'src/app/services/authentication.service';
import 'leaflet.tilelayer.pouchdbcached';
import PouchDB from 'pouchdb';

import { environment } from 'src/environments/environment';
import { first } from 'rxjs/operators';
import { Subject } from 'rxjs';

window['PouchDB'] = PouchDB;

export const DEFAULT_ZOOM = 8;
const STYLE_GEOLAYER: L.PathOptions = {
    color: '#0F7173',
    fillOpacity: 0.2
};

// override definition from pushDB, added " && !this.options.initPhase" condition for init use case
L.TileLayer.include({
    // Overwrites L.TileLayer.prototype.createTile
    createTile: function (coords, done) {
        const tile = document.createElement('img');
        
        L.DomEvent.on(tile, 'error', L.Util.bind(this._tileOnError, this, done, tile));
        
        if (this.options.crossOrigin) {
            tile.crossOrigin = '';
        }
        
        /*
         Alt tag is *set to empty string to keep screen readers from reading URL and for compliance reasons
         http://www.w3.org/TR/WCAG20-TECHS/H67
         */
        tile.alt = '';
        
        const tileUrl = this.getTileUrl(coords);
        
        if (this.options.useCache && !this.options.initPhase) {
            this._db.get(
                tileUrl,
                {revs_info: true},
                this._onCacheLookup(tile, tileUrl, done)
            );
        } else {
            // Fall back to standard behaviour
            L.DomEvent.on(tile, 'load', L.Util.bind(this._tileOnLoad, this, done, tile));
            tile.src = tileUrl;
        }
        
        return tile;
    }
});

@Component({
    selector: 'app-osm',
    templateUrl: './osm.component.html',
    styleUrls: ['./osm.component.scss']
})
export class OsmComponent implements OnInit, OnChanges, AfterViewInit, OnFilterHandler {
    public static readonly BASE_URL: String = environment.baseUrlApi;
    @ViewChild('mapDiv', {static: true}) mapContainer: ElementRef<HTMLDivElement>;
    
    /**
     * Event emitters
     */
    @Output() openFilter: EventEmitter<boolean> = new EventEmitter<boolean>();
    @Output() selectedMarker: EventEmitter<DeviceMarker> = new EventEmitter<DeviceMarker>();
    @Output() drawnGeometries = new EventEmitter<Geometry[]>();
    
    /**
     * Activation of features
     */
    @Input() measurementTools = false;
    @Input() drawPolygonTools = false;
    @Input() showFilter = false;
    
    /**
     * Data to be displayed or set
     */
    @Input() item;
    @Input() items: Device[] = [];
    @Input() locked = false;
    @Input() coordinates: Coordinates;
    @Input() currentFilters: Filter[] = [];
    @Input() control?: ControlPreparation;
    @Input() geoJsons?: FeatureCollection[];
    @Input() popupMode = false;
    
    /**
     * value is from 0 to 18
     */
    @Input() zoom?: number = DEFAULT_ZOOM;
    
    /**
     * Internal usage
     */
    private map: L.Map;
    private currentPositionMarker: L.Marker;
    private markerClusterGroup: L.MarkerClusterGroup;
    private markers: L.Marker[] = [];
    private layerSwitcher: any;
    private drawControl: DrawButton;
    private osmMoveHelper: OsmMoveHelper;
    private controlLayers: L.Layer[] = [];
    private controlAdded = false;
    private _filterButtonControl: FilterButton;
    private mapReady = new Subject<void>();
    
    constructor(private subjectsService: SubjectsService,
                private coordinatesService: CoordinatesService,
                private layerService: LayerService,
                private geometryService: GeometryService,
                private authenticationService: AuthenticationService) {
    }
    
    ngOnInit(): void {
        if (this.zoom < 0) {
            this.zoom = 0;
        } else if (this.zoom > 18) {
            this.zoom = 18;
        }
        this.coordinatesService.coordinates.subscribe(coordinates => this.coordinates = coordinates);
        this.subjectsService.getFilterModaleFromHiddenList().subscribe({
            next: () => this.onFilterClicked()
        });
    }
    
    ngAfterViewInit(): void {
        this.init();
        this.onSendWithOfContainer();
    }
    
    @HostListener('window:resize', ['$event'])
    onViewResize(event: Event) {
        this.onSendWithOfContainer();
    }
    
    onSendWithOfContainer(): void {
        this.mapContainer.nativeElement.addEventListener('resize', this.onViewResize.bind(this));
        const container = this.mapContainer.nativeElement.getBoundingClientRect();
        this.subjectsService.sendContainerFilterWidth(String(container.width));
    }
    
    ngOnChanges(changes: SimpleChanges): void {
        if (changes.items && !changes.items.isFirstChange()) {
            const previousValue: Device[] = changes.items.previousValue;
            const currentValue: Device[] = changes.items.currentValue;
            
            let toRemoveDevice: Device[] = previousValue;
            if (currentValue.length > 0) {
                toRemoveDevice = previousValue.filter((prevDevice) =>
                    currentValue.every((currDevice) => !(prevDevice.id === currDevice.id && prevDevice.status === currDevice.status))
                );
            }
            const toRemove: L.Marker[] = [];
            
            toRemoveDevice.forEach((device) => {
                const existingMarker: L.Marker[] = this.markers.filter((marker) => (<DeviceMarker>marker).getId() === device.id);
                if (existingMarker.length > 0) {
                    toRemove.push(existingMarker.pop());
                }
            });
            
            if (!this.markerClusterGroup) {
                this.markerClusterGroup = this.initMarkersClusters(this.map);
            }
            this.markerClusterGroup.removeLayers(toRemove);
            this.markers = this.markers.filter((marker) => !toRemove.includes(marker));
            this.addItems(currentValue.filter((prevDevice) =>
                previousValue.every((currDevice) => !(prevDevice.id === currDevice.id && prevDevice.status === currDevice.status))
            ));
        }
        if (this.control && this.control.geoJson && this.drawControl && !this.controlAdded) {
            this.drawControl.addGeoJSONToMapAsDrawnLayer(this.control.geoJson);
            this.controlAdded = true;
        }
        if (!this.drawPolygonTools && this.drawControl) {
            this.map.removeControl(this.drawControl);
        }
        this.controlLayers.forEach(layer => this.map.removeLayer(layer));
        if (this.geoJsons) {
            this.controlLayers = [];
            this.geoJsons.forEach(geoJson => this.controlLayers.push(L.geoJSON(geoJson, <L.GeoJSONOptions>{pmIgnore: true})
                .setStyle(STYLE_GEOLAYER).addTo(this.map)));
        } else {
            this.controlLayers = [];
        }
    }
    
    
    private init(): void {
        this.initMap();
        this.coordinatesService.locationCoordinates.subscribe(location => this.updateLocation(location)); // there is a loop here ?!?!
        if (this.coordinatesService.locationCoordinatesValue) {
            this.updateLocation(this.coordinatesService.locationCoordinatesValue);
        }
        this._filterButtonControl = filterButton(this.map, this, {position: 'topright'});
        if (this.showFilter) {
            this._filterButtonControl.enable();
        }
        
        this.layerSwitcher = LExtended.geoportalControl.LayerSwitcher({
            position: 'topright'
        });
        this.layerSwitcher.addTo(this.map);
        this.layerSwitcher.getContainer().classList.add('leaflet-control-custom');
        
        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', <any>{
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
            zIndex: 0,
            useCache: true,
            saveToCache: false,
            reuseTiles: true,
            queueLength: 1000
        }).addTo(this.map);
        
        const box = this.authenticationService.currentUserBox2154;
        this.layerService.findAll(box).subscribe({
            next: layers => {
                const layersForSwitcher = this.convertToLayers(layers);
                layersForSwitcher.forEach(value => {
                    this.layerSwitcher.addLayer(value.layer, {config: value.config});
                    (value.layer as any).options.initPhase = false;
                });
            }
        });
        
        this.map.addControl(myPositionButton(this.map, this.coordinatesService, {position: 'bottomright'}));
        
        if (this.measurementTools) {
            this.map.addControl(measurementsButton({position: 'topright'}));
            this.layerSwitcher.getContainer().classList.add('second-column');
        }
        if (this.drawPolygonTools) {
            this.drawControl = this.initDrawControl();
            if (!this.controlAdded && this.control && this.control.geoJson) {
                this.drawControl.addGeoJSONToMapAsDrawnLayer(this.control.geoJson);
                this.controlAdded = true;
            }
        } else if (this.control) {
            L.geoJSON(this.control.geoJson).setStyle(STYLE_GEOLAYER).addTo(this.map);
        }
        
        if (this.items.length > 0) {
            this.initItems();
        }
        
        this.osmMoveHelper = new OsmMoveHelper(this.map, this.coordinatesService);
        
        if (this.item) {
            this.initSelectPosition();
        }
    }
    
    private initDrawControl() {
        if (this.drawControl) {
            return this.drawControl;
        }
        const drawButtonInstance = drawButton({
            position: 'topleft', alwaysOpen: true, drawPolygon: true, pathOptions: STYLE_GEOLAYER,
            persistDrawnGeometries: this.updateDrawnGeometries.bind(this)
        });
        this.map.addControl(drawButtonInstance);
        return drawButtonInstance;
    }
    
    private updateDrawnGeometries(geometries: Geometry[]) {
        this.geometryService.updateDrawnGeometries(geometries);
        this.drawnGeometries.emit(geometries);
    }
    
    public getOsmMoveHelper(): OsmMoveHelper {
        return this.osmMoveHelper;
    }
    
    private updateLocation(currentPosition: Coordinates): void {
        if (!this.currentPositionMarker) {
            this.currentPositionMarker = L.marker([currentPosition.latitude, currentPosition.longitude], <L.MarkerOptions>{
                pmIgnore: true,
                icon: L.divIcon({
                    iconSize: [20, 20],
                    iconAnchor: [10, 10],
                    html: '<div class="current-position-bg"><div class="current-position-icon"></div></div>'
                })
            });
            this.currentPositionMarker.addTo(this.map);
        } else {
            this.currentPositionMarker.setLatLng({lat: currentPosition.latitude, lng: currentPosition.longitude});
        }
    }
    
    private initMap(): void {
        if (this.popupMode) {
            L.DomEvent.on(<any>window, 'animationstart', this.fixMapPositionInPopIn, this);
            L.DomEvent.on(<any>window, 'animationiteration', this.fixMapPositionInPopIn, this);
            L.DomEvent.on(<any>window, 'animationend', this.fixMapPositionInPopIn, this);
        }
        const box = this.authenticationService.currentUserBox4326;
        const southWest = L.latLng(box.minLat - 0.2, box.minLong - 0.2);
        const northEast = L.latLng(box.maxLat + 0.2, box.maxLong + 0.2);
        this.map = L.map(this.mapContainer.nativeElement, {
            zoomControl: false
        }).fitBounds(L.latLngBounds(southWest, northEast))
            .addControl(this.zoomControl());
    }
    
    /**
     * call this method when a transition (animation) delay the showing of the map
     * like in the popin where there is a fadeIn animation
     * invalidateSize repositionate the background in the actual visible div
     * fitBounds zoom to fit with the box of the current user
     */
    private fixMapPositionInPopIn() {
        const box = this.authenticationService.currentUserBox4326;
        const southWest = L.latLng(box.minLat - 0.2, box.minLong - 0.2);
        const northEast = L.latLng(box.maxLat + 0.2, box.maxLong + 0.2);
        this.map.invalidateSize()
            .fitBounds(L.latLngBounds(southWest, northEast));
        L.DomEvent.off(<any>window, 'animationstart', this.fixMapPositionInPopIn, this);
        L.DomEvent.off(<any>window, 'animationiteration', this.fixMapPositionInPopIn, this);
        L.DomEvent.off(<any>window, 'animationend', this.fixMapPositionInPopIn, this);
    }
    
    private positionAt(position: Coordinates, zoom = this.zoom) {
        if (this.map) {
            this.map.setView([position.latitude, position.longitude], zoom);
        }
    }
    
    private initSelectPosition(): void {
        if (this.item.id) {
            this.initLockMapButton(this.coordinates);
        } else {
            //initialisation de la valeur
            this.initLockMapButton(this.coordinatesService.locationCoordinatesValue);
            this.coordinatesService.locationCoordinates.pipe(first()).subscribe({
                next: value => this.initLockMapButton(value)
            });
        }
    }
    
    
    private initLockMapButton(value) {
        this.positionAt(value);
        lockMapButton({
            trackedMarker: L.marker([value.latitude, value.longitude], {
                icon: deviceIcon(this.item.status, this.item.type)
            }),
            updateCoordinates: this.coordinatesService.updateCoordinates.bind(this.coordinatesService),
            isLocked: this.locked,
            position: 'bottomright',
        }).addTo(this.map);
    }
    
    private zoomControl(): L.Control.Zoom {
        return L.control
            .zoom({zoomInTitle: 'Zoomer', zoomOutTitle: 'Dézoomer'})
            .setPosition('bottomright');
    }
    
    private addItems(devices: Device[]) {
        const markers: L.Marker[] = this.markersFrom(devices);
        this.markers = this.markers.concat(markers);
        this.markerClusterGroup.addLayers(markers);
    }
    
    private markersFrom(devices: Device[]): L.Marker[] {
        const myFeatureGroup = L.featureGroup().on('click', function (event) {
            const clickedMarker: DeviceMarker = event['layer'];
            const latLng: L.LatLng = clickedMarker.getLatLng();
            const markerCoordinates = <Coordinates>{
                latitude: latLng.lat,
                longitude: latLng.lng,
                id: clickedMarker.getId()
            };
            this.positionAt(markerCoordinates, this.map.getZoom());
            this.onSelectMarker(clickedMarker);
            this.selectedMarker.emit(clickedMarker);
        }.bind(this));
        const markers: L.Marker[] = [];
        devices.forEach((item) => {
            let status = item.status;
            if (item.inBackground) {
                status = Status.NOTSELECTED;
            }
            const marker = deviceMarker(item, [item.coordinates.latitude, item.coordinates.longitude], <L.MarkerOptions>{
                pmIgnore: true,
                icon: deviceIcon(status, item.type)
            }).addTo(myFeatureGroup);
            markers.push(marker);
        });
        return markers;
    }
    
    private onSelectMarker(selectedMarker: DeviceMarker) {
        selectedMarker.setIcon(deviceIcon(selectedMarker.getStatus(), selectedMarker.getType()));
        this.markers.forEach((marker: DeviceMarker) => {
            if (marker.getId() !== selectedMarker.getId()) {
                marker.setIcon(deviceIcon(Status.NOTSELECTED, marker.getType()));
            }
        });
    }
    
    public resetMarkerState() {
        if (this.markers) {
            this.markers.forEach((marker: DeviceMarker) => {
                marker.setIcon(deviceIcon(marker.getStatus(), marker.getType()));
            });
        }
    }
    
    private initItems() {
        this.markers = this.markersFrom(this.items);
        this.markerClusterGroup = this.initMarkersClusters(this.map);
        this.markerClusterGroup.addLayers(this.markers);
    }
    
    private initMarkersClusters(aMap: L.Map): L.MarkerClusterGroup {
        const markerClusterGroup: L.MarkerClusterGroup = deviceMarkerClusterGroup();
        aMap.addLayer(<L.Layer>markerClusterGroup);
        return markerClusterGroup;
    }
    
    public onFilterClicked() {
        this.openFilter.emit(true);
    }
    
    private convertToLayers(layers: LayerDefinition[]): { layer: L.Layer, display?: boolean, config?: { [name: string]: any } }[] {
        const result: { layer: L.Layer, config: { [name: string]: any } }[] = [];
        
        layers.forEach(layerDef => {
            let url;
            if (layerDef.needProxy) {
                url = `${OsmComponent.BASE_URL}/layer/proxy/${layerDef.id}`;
            } else {
                url = layerDef.url;
            }
            const wmsLayer = L.tileLayer.wms(url, <L.WMSOptions>{
                layers: layerDef.parameters.typeName,
                format: layerDef.parameters.outputFormat,
                transparent: true,
                opacity: 0.2,
                useCache: true,
                saveToCache: false,
                reuseTiles: true,
                queueLength: 1000,
                initPhase: true // is just here to prevent pushDB fetching data when hidden at startup
            });
            result.push({
                layer: wmsLayer,
                config: {title: layerDef.name, description: layerDef.name, visibility: false}
            });
        });
        return result;
    }
    
    public get filterButtonControl() {
        return this._filterButtonControl;
    }
}

