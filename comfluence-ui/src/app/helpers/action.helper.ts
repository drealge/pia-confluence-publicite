import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { first, tap } from 'rxjs/operators';
import { ChangeType } from '../models/ChangeType';
import { Device } from '../models/device/device';
import { Document } from '../models/device/document';
import { InfrigementStatus } from '../models/device/properties/infrigement';
import { PhotoStatus } from '../models/device/properties/photo';
import { DeviceService } from '../services/device.service';
import { PhotoService } from '../services/photo.service';
import { StateService } from '../services/state.service';
import { DocumentService } from '../shared/services/document.service';
import { ActionsConstantes } from './action.constants';
import { SaveRequestHelper } from './request/saverequest.helper';

export interface Cachable {
  unSyncId: number;
  sync: boolean;
}

export class ActionDto {
  action: string;
  data?: any;
}

export class CachedHttpError extends HttpErrorResponse {
  cached = false;
}

@Injectable({
  providedIn: 'root'
})
export class ActionHelper {

  constructor(private deviceService: DeviceService,
    private photoService: PhotoService,
    private saveRequestHelper: SaveRequestHelper,
    private stateService: StateService,
    private documentService: DocumentService) {
  }

  public doAction(action: string, data: any): Observable<any> {
    let result: Observable<any>;
    switch (action) {
      case ActionsConstantes.ACTION_DEVICE_SAVE:
        result = this.saveDeviceAction(data);
        break;
      case ActionsConstantes.ACTION_PHOTO_SAVE:
        result = this.photoService.postPhoto(data);
        break;
      case ActionsConstantes.ACTION_PHOTO_DELETE:
        result = this.photoService.deletePhoto(data);
        break;
      case ActionsConstantes.ACTION_DEVICE_REMOVE:
        result = this.deviceService.remove(data);
        break;
      case ActionsConstantes.ACTION_INFRIGEMENT_DELETE:
        result = this.documentService.save(data);
        break;
      default:
        result = new Observable(subscriber => subscriber.next(undefined));
        break;
    }
    return result.pipe(first(), tap(
      dataResult => {
        // nothing to do
      }, (error: CachedHttpError) => {
        if (!this.isRealError(error.status)) {
          error.cached = true;
          this.saveRequestHelper.saveRequest(action, data);
        }
      }
    ));
  }

  private saveDeviceAction(device: Device): Observable<any> {
    if (!device.unSyncId) {
      device.unSyncId = Date.now();
    }
    // remove the link of the deleted infrigements when saving
    const deletedInfrigements = device.infrigements.filter(infrigement => infrigement.status === InfrigementStatus.TO_DELETE);
    device.infrigements = device.infrigements.filter(infrigement => infrigement.status !== InfrigementStatus.TO_DELETE);
    return this.deviceService.save(device)
      .pipe(first(), tap(savedDevice => {
        console.log('Dispositif correctement sauvegardé');
        savedDevice.photos = device.photos
          .filter((photo) => photo.status === PhotoStatus.SAVED);
        device.photos
          .filter((photo) => photo.status === PhotoStatus.TO_SAVE)
          .forEach((photo) => {
            photo.idDispositif = savedDevice.id;
            this.doAction(ActionsConstantes.ACTION_PHOTO_SAVE, photo)
              .pipe(first()).subscribe(result => console.log('photo uploaded'));
          });
        device.photos
          .filter((photo) => photo.id && (photo.status === PhotoStatus.TO_DELETE))
          .forEach((photo) => this.doAction(ActionsConstantes.ACTION_PHOTO_DELETE, photo.id));

        deletedInfrigements.forEach(infrigement => {
          let comment = `Infraction : ${infrigement.offenseNature}`;
          if (infrigement.comment) {
            comment += `<br>${infrigement.comment}`;
          }
          const document = new Document();
          document.comment = comment;
          document.idDevice = savedDevice.id;
          document.changeType = ChangeType.SUSPENDED_INFRIGEMENT;

          this.doAction(ActionsConstantes.ACTION_INFRIGEMENT_DELETE, document)
            .pipe(first()).subscribe();
        });
        this.stateService.addOrUpdateDevice(savedDevice, device.unSyncId);
      }, error => {

        device.sync = false;
        this.stateService.addOrUpdateDevice(device, device.unSyncId);
      })
      );
  }

  private removeDevice(deviceId: number): Observable<Device> {
    return this.deviceService.remove(deviceId).pipe(first(),
      tap(device => {/* nothing to do */
      },
        error => {
          const device = this.stateService.findDevice(deviceId);
          device.removed = true;
          device.sync = false;
          device.unSyncId = Date.now();
          this.stateService.addOrUpdateDevice(device, device.unSyncId);
        }
      )
    );
  }

  private isRealError(httpError: number): boolean {
    return (httpError !== 0);
  }
}
