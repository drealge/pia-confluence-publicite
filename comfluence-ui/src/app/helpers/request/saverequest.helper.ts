import { Injectable } from '@angular/core';
import { StateService } from 'src/app/services/state.service';
import { RequestDatabase, RequestEntity } from './saverequest.db';

@Injectable({
    providedIn: 'root'
})
export class SaveRequestHelper {
    constructor(private requestDatabase: RequestDatabase, private stateService: StateService) {
    }
    saveRequest(action: string, data?: any) {
        const requestEntity: RequestEntity = {
            action: action,
            data: data
        };
        this.requestDatabase.requestTable.add(requestEntity)
            .then(key => {
                this.stateService.updateUnsyncValue(true);
                console.log('[ADDED]' + requestEntity.action + ' with key: ' + key);
            })
            .catch(error => console.error('[ADD]' + requestEntity.action));
    }
}
