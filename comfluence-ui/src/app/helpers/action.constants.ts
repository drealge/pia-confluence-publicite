
export class ActionsConstantes {
  static readonly ACTION_DEVICE_SAVE = 'action.device.save';
  static readonly ACTION_DEVICE_REMOVE = 'action.device.remove';
  static readonly ACTION_PHOTO_SAVE = 'action.photo.save';
  static readonly ACTION_PHOTO_DELETE = 'action.photo.delete';
  static readonly ACTION_INFRIGEMENT_DELETE = 'action.infrigement.delete';
}
