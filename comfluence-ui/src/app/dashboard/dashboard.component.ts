import { AfterViewInit, Component, ElementRef, HostListener, Inject, LOCALE_ID, OnDestroy, OnInit, ViewChildren } from '@angular/core';
import { Statistics } from 'src/app/models/device/Statistics';
import { DeviceService } from 'src/app/services/device.service';
import { StatisticsProcedure } from 'src/app/models/device/StatisticsProcedure';
import { environment } from 'src/app/../environments/environment';
import { PrefetchService, PrefetchState, PrefetchStatus } from 'src/app/services/prefetch.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { ActiveToast, ToastrService } from 'ngx-toastr';
import { Subscription, EMPTY } from 'rxjs';
import { ControlService } from 'src/app/services/control.service';
import * as L from 'leaflet';
import { createBox4326 } from 'src/app/models/box';
import { ServiceHelper } from '../helpers/service-helper';
import { saveAs } from 'file-saver';
import { DocumentService } from '../shared/services/document.service';
import { User } from '../models/user';
import { Department } from '../models/department';
import { DepartementService } from '../services/departement.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { formatDate } from '@angular/common';
import { map, first, catchError } from 'rxjs/operators';
import { CoordinatesService } from '../services/coordinates.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements AfterViewInit, OnDestroy {

  @ViewChildren('drawer', { read: ElementRef }) bottomDrawer: ElementRef;
  top = 0;
  fixeDrawer = false;
  modalType: number;
  modalIsOpen = false;
  today: number = Date.now();
  version = environment.version;
  outOfTimeTotal: number;
  outOfTime: StatisticsProcedure[];
  statistics: Statistics = <Statistics>{
    compliantDevices: 0,
    nonCompliantDevices: 0,
    todoDevices: 0
  };
  statisticsLastCallDays: number;
  private stateSubscription: Subscription;
  private prefetchToast: ActiveToast<any>;
  modalExportLicorne = false;
  currentUser: User;
  departments: Department[] = [];
  licorneExportForm: FormGroup;

  constructor(private deviceService: DeviceService,
    private prefetchService: PrefetchService,
    private authenticationService: AuthenticationService,
    private controlService: ControlService,
    private toastrService: ToastrService,
    private documentService: DocumentService,
    private departementService: DepartementService,
    private coordinateService: CoordinatesService,
    private formBuilder: FormBuilder,
    @Inject(LOCALE_ID) public locale: string) {
    this.stateSubscription = this.prefetchService.state.subscribe(state => {
      if (!state.init) {
        this.updatePrefetchState(state);
      }
    });
    this.currentUser = this.authenticationService.currentUserValue;
    this.departementService.findAll().pipe(map((data) => {
      data.sort((a, b) => {
        return a.code < b.code ? -1 : 1;
      });
      return data;
    })).subscribe(departments => this.departments = departments);
    const todayFormatedDate = formatDate(new Date(), 'yyyy-MM-dd', this.locale);
    this.licorneExportForm = formBuilder.group({
      depCode: formBuilder.control(this.currentUser.departement.code),
      dateMin: formBuilder.control(todayFormatedDate),
      dateMax: formBuilder.control(todayFormatedDate)
    });
    this.deviceService.statisticsDispositifs(this.currentUser.departement.code).subscribe((data) => {
      this.statistics = data;
      this.statisticsLastCallDays = Math.trunc((Date.now() - data.lastCall.getTime()) / 1000 / 60 / 60 / 24); // milli -> day
    });
    this.totalOutOfTime();
  }

  ngAfterViewInit(): void {
    this.bottomDrawer['first'].nativeElement.addEventListener('scroll', this.onDrawerScroll.bind(this));
  }

  @HostListener('window:scroll', ['$event'])
  onDrawerScroll(event: Event) {
    const drawer = this.bottomDrawer['first'].nativeElement.getBoundingClientRect();
    this.fixeDrawer = drawer.y < 25;
  }

  totalOutOfTime() {
    this.deviceService.statisticsProcedures(this.currentUser.departement.code)
      .subscribe(statisticProcedureList => {
        this.outOfTime = [];
        if (statisticProcedureList.length > 0) {
          const eachQuantity = [];
          statisticProcedureList.forEach(el => {
            if (el.quantity > 0) {
              this.outOfTime.push(el);
            }
            eachQuantity.push(el.quantity);
          });
          this.outOfTimeTotal = eachQuantity.reduce((acc: number, curr: number) => acc + curr);
        }
      });
  }

  loadCurrentPreparedControl(): void {
    this.controlService.findActiveByUser(this.currentUser).pipe(first()).subscribe(control => {
      let box;
      if (control.geoJson && control.geoJson.features) {
        const bounds = L.geoJSON(control.geoJson).getBounds();
        bounds.pad(0.05);
        const northEast = bounds.getNorthEast();
        const southWest = bounds.getSouthWest();
        box = createBox4326({
          minLat: southWest.lat,
          minLong: southWest.lng,
          maxLat: northEast.lat,
          maxLong: northEast.lng
        });
      } else {
        box = this.coordinateService.boxValue;
      }
      this.prefetchService.prefetchRessources(box, this.currentUser.departement.code);
    }, error => {
      this.prefetchService.prefetchRessources(this.coordinateService.boxValue, this.currentUser.departement.code);
    });
  }

  showModal(type: number): void {
    this.modalIsOpen = true;
    this.modalType = type;
  }

  closeModal(toggle: boolean): void {
    this.modalIsOpen = toggle;
  }

  private updatePrefetchState(prefetchState: PrefetchState) {

    if (this.giveGlobalState(prefetchState) === PrefetchStatus.DONE) {
      if (this.prefetchToast) {
        this.toastrService.clear(this.prefetchToast.toastId);
        this.prefetchToast = undefined;
      }
      this.toastrService.success('Terminée', 'Synchronisation des données', {
        enableHtml: true,
      });
    } else if (this.giveGlobalState(prefetchState) === PrefetchStatus.ERROR) {
      if (this.prefetchToast) {
        this.toastrService.clear(this.prefetchToast.toastId);
        this.prefetchToast = undefined;
      }
      this.toastrService.error('Erreur', 'Synchronisation des données', {
        enableHtml: true,
      });
    } else {
      const deviceMsg = 'Chargement des dispositifs: ' + this.translateState(prefetchState.deviceStatus)
        + ' (' + prefetchState.nbDeviceUpdated + '/' + prefetchState.nbDeviceToUpdate + ')';
      const layerMsg = 'Chargement des couches: ' + this.translateState(prefetchState.layersState.layersStatus)
        + ' (' + prefetchState.layersState.layersDone + '/' + prefetchState.layersState.totalLayers + ')';
      const procedureMsg = 'Chargement de la liste des annonceurs: ' + this.translateState(prefetchState.advertiserStatus);
      const filtreMsg = 'Chargement de la liste des filtres: ' + this.translateState(prefetchState.filterStatus);
      const message = deviceMsg + '<br/>'
        + layerMsg + '<br/>'
        + procedureMsg + '<br/>'
        + filtreMsg;
      if (this.prefetchToast) {
        this.prefetchToast.toastRef.componentInstance.message = message;
      } else {
        this.prefetchToast = this.toastrService.info(message, 'Synchronisation des données', {
          enableHtml: true,
          disableTimeOut: true,
          easeTime: 0,
        });
      }
    }

  }

  private giveGlobalState(state: PrefetchState) {
    let result = state.deviceStatus;
    if (result === PrefetchStatus.DONE && state.nbDeviceToUpdate > state.nbDeviceUpdated) {
      result = PrefetchStatus.STARTED;
    }
    if (result !== PrefetchStatus.STARTED) {
      result = state.filterStatus;
    }
    if (result !== PrefetchStatus.STARTED) {
      result = state.layersState.layersStatus;
    }
    if (result !== PrefetchStatus.STARTED) {
      result = state.advertiserStatus;
    }
    if (result === PrefetchStatus.DONE &&
      (state.deviceStatus === PrefetchStatus.ERROR ||
        state.filterStatus === PrefetchStatus.ERROR ||
        state.layersState.layersStatus === PrefetchStatus.ERROR ||
        state.advertiserStatus === PrefetchStatus.ERROR)) {
      result = PrefetchStatus.ERROR;
    }
    return result;
  }

  private translateState(status: PrefetchStatus) {
    let result: string;
    switch (status) {
      case PrefetchStatus.DONE:
        result = 'Terminé';
        break;
      case PrefetchStatus.ERROR:
        result = 'Erreur';
        break;
      case PrefetchStatus.STARTED:
        result = 'En cours';
        break;
      default:
        result = 'En cours';
        break;
    }
    return result;
  }

  ngOnDestroy(): void {
    this.stateSubscription.unsubscribe();
  }

  saveLicorneExport(value: any) {
    this.documentService.licorneExport(value.depCode, value.dateMin, value.dateMax).subscribe(response => {
      const fileName = ServiceHelper.getFileNameFromResponseContentDisposition(response);
      saveAs(response.body, fileName);
    });
  }
}
