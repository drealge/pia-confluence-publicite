declare module GeoApi {

    export interface Commune {
        code: string;
        nom: string;
        codesPostaux: string[];
        population: number;
    }
}

export { GeoApi };
