export enum ChangeType {
  MODIFIED = 'MODIFIED',
  ADDED = 'ADDED',
  SUSPENDED_INFRIGEMENT = 'SUSPENDED_INFRIGEMENT'
}
