import { Address } from './properties/address';
import { Photo } from './properties/photo';
import { Infrigement } from './properties/infrigement';
import { Context } from './properties/context';
import { Description } from './properties/description';
import { Comment } from './properties/comment';
import { Coordinates } from './properties/coordinates';
import { Filter } from './Filter';
import { Cachable } from 'src/app/helpers/action.helper';

export class Device implements Cachable {
  id: number;
  reference: string;
  type: Type;
  guidance: Guidance;
  status: Status;
  address: Address;
  coordinates: Coordinates;
  context: Context;
  description: Description;
  infrigements: Infrigement[] = [];
  comment: Comment;
  photos: Photo[] = [];
  operationDate: Date;
  tags: Filter[] = [];
  removed = false;
  sync = false;
  unSyncId = -1;
  lastProcGroupId: number[] = [];
  inBackground = false;
}

export enum Type {
  AD = 'AD',
  SIGNBOARD = 'SIGNBOARD',
  PRESIGNBOARD = 'PRESIGNBOARD'
}

export enum Guidance {
  AUTHORIZATION = 'AUTHORIZATION',
  DECLARATION = 'DECLARATION',
  OUTSTATE = 'OUTSTATE'
}

export enum Status {
  COMPLIANT = 'COMPLIANT',
  NONCOMPLIANT = 'NONCOMPLIANT',
  TODO = 'TODO',
  NOTSELECTED = 'NOTSELECTED'
}
