export class Context {
  id: number;
  town: string;
  population: number;
  unit: string;
  constraints: string[];
  location: Location;
}

export enum Location {
  AGGLOMERATION = 'AGGLOMERATION',
  COUNTRYSIDE = 'COUNTRYSIDE',
  UNDEFINED_LOCATION = 'UNDEFINED_LOCATION'
}
