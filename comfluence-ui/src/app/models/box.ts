import proj4 from 'proj4';
// south east : lat = minLat, long = maxLong
// south west : lat = minLat, long = minLong
// north west : lat = maxLat, long = minLong
// north east : lat = maxLat, long = maxLong
// Latitude = haut/bas (-/+), longitude = gauche/droite (-/+)

export interface Box {
  minLat: number;
  maxLat: number;
  minLong: number;
  maxLong: number;
}

export class Box4326 implements Box {
  constructor() {
  }

  // format EPSG:4326 WGS 84
  minLat: number;
  maxLat: number;
  minLong: number;
  maxLong: number;

  fromBox2154(box: Box2154) {
    const convertMin = proj4('EPSG:2154', 'EPSG:4326', [box.maxLong, box.minLat]);
    this.minLat = convertMin[0];
    this.maxLong = convertMin[1];
    const convertMax = proj4('EPSG:2154', 'EPSG:4326', [box.minLong, box.maxLat]);
    this.maxLat = convertMax[0];
    this.minLong = convertMax[1];
  }
}

export function createBox4326(box: Box): Box4326 {
  const result = new Box4326();
  result.maxLat = box.maxLat;
  result.minLat = box.minLat;
  result.maxLong = box.maxLong;
  result.minLong = box.minLong;
  return result;
}

export class Box2154 implements Box {
  // EPSG:2154 RGF93 / Lambert-93
  minLat: number;
  maxLat: number;
  minLong: number;
  maxLong: number;

  static fromBox3857(box: Box3857) {
    const resultBox = new Box2154();
    const convertMin = proj4('EPSG:3857', 'EPSG:2154', [box.maxLong, box.minLat]);
    resultBox.minLat = convertMin[1];
    resultBox.maxLong = convertMin[0];
    const convertMax = proj4('EPSG:3857', 'EPSG:2154', [box.minLong, box.maxLat]);
    resultBox.maxLat = convertMax[1];
    resultBox.minLong = convertMax[0];
    return resultBox;
  }

  static fromBox4326(box: Box4326) {
    const resultBox = new Box2154();
    const convertMin = proj4('EPSG:4326', 'EPSG:2154', [box.maxLong, box.minLat]);
    resultBox.minLat = convertMin[1];
    resultBox.maxLong = convertMin[0];
    const convertMax = proj4('EPSG:4326', 'EPSG:2154', [box.minLong, box.maxLat]);
    resultBox.maxLat = convertMax[1];
    resultBox.minLong = convertMax[0];
    return resultBox;
  }
}

export function createBox2154(box: Box): Box2154 {
  const result = new Box2154();
  result.maxLat = box.maxLat;
  result.minLat = box.minLat;
  result.maxLong = box.maxLong;
  result.minLong = box.minLong;
  return result;
}

export class Box3857 implements Box {
  // EPSG:3857
  minLat: number;
  maxLat: number;
  minLong: number;
  maxLong: number;

  static fromBox4326(box: Box4326) {
    const resultBox = new Box3857();
    const convertMin = proj4('EPSG:4326', 'EPSG:3857', [box.maxLong, box.minLat]);
    resultBox.minLat = convertMin[0];
    resultBox.maxLong = convertMin[1];
    const convertMax = proj4('EPSG:4326', 'EPSG:3857', [box.minLong, box.maxLat]);
    resultBox.maxLat = convertMax[0];
    resultBox.minLong = convertMax[1];
    return resultBox;
  }

  static fromBox2154(box: Box2154) {
    const resultBox = new Box3857();
    const convertMin = proj4('EPSG:2154', 'EPSG:3857', [box.maxLong, box.minLat]);
    resultBox.minLat = convertMin[1];
    resultBox.maxLong = convertMin[0];
    const convertMax = proj4('EPSG:2154', 'EPSG:3857', [box.minLong, box.maxLat]);
    resultBox.maxLat = convertMax[1];
    resultBox.minLong = convertMax[0];
    return resultBox;
  }
}
