import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, mergeMap, tap } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { MergeRequestHelper } from '../helpers/request/mergerequest.helper';
import { Box4326 } from '../models/box';
import { Device } from '../models/device/device';
import { Statistics } from '../models/device/Statistics';
import { StatisticsProcedure } from '../models/device/StatisticsProcedure';
import { StateService } from './state.service';
import {response} from "express";

@Injectable({
  providedIn: 'root'
})
export class DeviceService {

  static readonly BASE_URL: String = environment.baseUrlApi;

  constructor(private httpClient: HttpClient, private stateService: StateService, private mergeRequestHelper: MergeRequestHelper) {
  }

  findAllFromBox(box: Box4326, filters: number[] = [], forceUpdate = false): Observable<Device[]> {
    const localForceUpdate = forceUpdate || (filters && filters.length > 0);
    if (!localForceUpdate && this.isBoxInAndSmaller(box)) {
      return new Observable<Device[]>(subscriber => subscriber.next(this.findAllCached(box)));
    } else {
      let httpParam = new HttpParams({
        fromObject: {
          minLat: box.minLat.toString(),
          maxLat: box.maxLat.toString(),
          minLong: box.minLong.toString(),
          maxLong: box.maxLong.toString(),
        }
      });
      httpParam = httpParam.append('appliedFilters', filters.join(','));
      return this.httpClient.get<Device[]>(`${DeviceService.BASE_URL}/dispositif/box`, { params: httpParam })
          .pipe(
              tap(response => {
                  this.stateService.updateLastBox(box);
                  this.stateService.updatelastDevices(response);
              })
          );
    }
  }

  findAll(): Observable<Device[]> {
    return this.httpClient.get<Device[]>(`${DeviceService.BASE_URL}/dispositif`)
      .pipe(tap(devices => devices.forEach(device => this.fixModel(device))));
  }

  statisticsDispositifs(iddep: number): Observable<Statistics> {
    return this.httpClient.get<Statistics>(`${DeviceService.BASE_URL}/statistics/${iddep}/dispositifs`).pipe(map(statistics => {
      statistics.lastCall = new Date(statistics.lastCall);
      return statistics;
    }));
  }
  statisticsProcedures(iddep: number): Observable<StatisticsProcedure[]> {
    return this.httpClient.get<StatisticsProcedure[]>(`${DeviceService.BASE_URL}/statistics/${iddep}/procedures`);
  }

  get(id: number): Observable<Device> {
    return this.httpClient.get<Device>(`${DeviceService.BASE_URL}/dispositif/${id}`)
      .pipe(mergeMap(device => this.mergeRequestHelper.mergeWithSavedDevice(device)));
  }

  save(device: Device): Observable<Device> {
    return this.httpClient.post<Device>(`${DeviceService.BASE_URL}/dispositif`, device);
  }

  remove(id: number): Observable<Device> {
    return this.httpClient.put<Device>(`${DeviceService.BASE_URL}/dispositif/${id}/remove`, '');
  }

  delete(id: number): Observable<Device> {
    return this.httpClient.delete<Device>(`${DeviceService.BASE_URL}/dispositif/${id}`);
  }

  findAllByControlId(idControl: number): Observable<Device[]> {
    return this.httpClient.get<Device[]>(`${DeviceService.BASE_URL}/dispositif/byControl/${idControl}`);
  }

  private findAllCached(box: Box4326): Device[] {
    return this.stateService.lastDevices.filter(
      device => this.isPointInside(device.coordinates.latitude, device.coordinates.longitude, box)
    );
  }

  private isBoxInAndSmaller(newBox: Box4326): boolean {
    const lastBox = this.stateService.lastBox;
    return this.isPointInside(newBox.minLat, newBox.minLong, lastBox) &&
      this.isPointInside(newBox.maxLat, newBox.maxLong, lastBox);
  }

  private isPointInside(latTarget: number, longTarget: number, box: Box4326) {
    return (latTarget >= box.minLat && latTarget <= box.maxLat &&
      longTarget >= box.minLong && longTarget <= box.maxLong);
  }

  private fixModel(device: Device) {
    device.operationDate = new Date(device.operationDate);
  }
}
