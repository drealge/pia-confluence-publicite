import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Geometry } from 'geojson';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { City } from '../models/control/city';

@Injectable({
  providedIn: 'root'
})
export class GeometryService {
  static readonly BASE_URL: String = environment.baseUrlApi;
  private drawnGeometriesSubject: BehaviorSubject<Geometry[]>;
  public drawnGeometries: Observable<Geometry[]>;

  constructor(private httpClient: HttpClient) {
    this.drawnGeometriesSubject = new BehaviorSubject<Geometry[]>([]);
    this.drawnGeometries = this.drawnGeometriesSubject.asObservable();
  }

  updateDrawnGeometries(geometries: Geometry[]) {
    this.drawnGeometriesSubject.next(geometries);
  }

  public get drawnGeometriesValue(): Geometry[] {
    return this.drawnGeometriesSubject.value;
  }

  findIntersectingCities(geometries: Geometry[]): Observable<City[]> {
    return this.httpClient.post<City[]>(`${GeometryService.BASE_URL}/city/byGeometries`,
      geometries.map(geometry => JSON.stringify(geometry)));
  }
}
