import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { Coordinates } from '../models/device/properties/coordinates';
import { debounceTime } from 'rxjs/operators';
import { Box4326 } from '../models/box';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class CoordinatesService {
  private coordinatesSubject: BehaviorSubject<Coordinates>;
  // coordonnées du point
  public coordinates: Observable<Coordinates>;
  // autre coordonnées ...
  public locationCoordinates: Observable<Coordinates>;
  public locationCoordinatesValue: Coordinates;
  public currentBox: Observable<Box4326>;
  private currentBoxSubject: BehaviorSubject<Box4326>;

  constructor(private httpClient: HttpClient, private authenticationService: AuthenticationService) {
    this.currentBoxSubject = new BehaviorSubject<Box4326>(authenticationService.currentUserBox4326);
    this.currentBox = this.currentBoxSubject.pipe(debounceTime(1000));

    this.locationCoordinatesValue = {
      latitude: (authenticationService.currentUserBox4326.maxLat + authenticationService.currentUserBox4326.minLat) / 2,
      longitude: (authenticationService.currentUserBox4326.maxLong + authenticationService.currentUserBox4326.minLong) / 2
    };
    this.coordinatesSubject = new BehaviorSubject<Coordinates>(this.locationCoordinatesValue);
    this.coordinates = this.coordinatesSubject.pipe(debounceTime(1000));

    this.locationCoordinates = new Observable<Coordinates>(observer => {
      if (window.navigator && window.navigator.geolocation) {
        window.navigator.geolocation.watchPosition(
          (position) => {
              this.locationCoordinatesValue = {latitude: position.coords.latitude, longitude: position.coords.longitude};
            observer.next(this.locationCoordinatesValue);
            observer.complete();
          },
          (error: PositionError) => {
            console.log(error);
            console.log('Error : fallback to default position');
            observer.next(this.locationCoordinatesValue);
            observer.complete();
          }
        );
      } else {
        console.error('Unsupported Browser : fallback to default position');
        observer.next(this.locationCoordinatesValue);
        observer.complete();
      }
    });
  }

  public updateBox(box: Box4326) {
    this.currentBoxSubject.next(box);
  }

  public get coordinatesValue(): Coordinates {
    return this.coordinatesSubject.value;
  }

  public updateCoordinates(coordinates: Coordinates) {
    this.coordinatesSubject.next(coordinates);
  }

  public get boxValue(): Box4326 {
    return this.currentBoxSubject.value;
  }
}
