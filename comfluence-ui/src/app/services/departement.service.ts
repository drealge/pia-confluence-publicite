import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Department, FullDepartment } from '../models/department';
import { BaseService } from '../shared/services/base.service';

@Injectable({
  providedIn: 'root'
})
export class DepartementService extends BaseService<Department> {
  constructor(private httpClient: HttpClient) {
    super('departement', httpClient);
  }

  findAllFull(): Observable<FullDepartment[]> {
    return this.httpClient.get<FullDepartment[]>(`${this.baseUrl}/full`);
  }
  save(department: Department): Observable<Department> {
    return this.httpClient.post<Department>(this.baseUrl, department);
  }
  saveFull(department: FullDepartment): Observable<FullDepartment> {
    return this.httpClient.post<FullDepartment>(`${this.baseUrl}/full`, department);
  }
}
