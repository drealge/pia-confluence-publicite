import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { RetryRequestHelper } from '../helpers/request/requestcache.helper';

@Injectable({
  providedIn: 'root'
})
export class NetworkStatusService {
  private onLineSubject: BehaviorSubject<boolean>;
  public onLine: Observable<boolean>;

  constructor(private retryRequestHelper: RetryRequestHelper) {
    this.onLineSubject = new BehaviorSubject(navigator.onLine);
    this.onLine = this.onLineSubject.asObservable();
    window.addEventListener('online', () => this.updateOnlineStatus(true));
    window.addEventListener('offline', () => this.updateOnlineStatus(false));
  }

  public isNetworkOnline(): boolean {
    return this.onLineSubject.getValue();
  }

  public isNetworkOffLine(): boolean {
    return !this.isNetworkOnline();
  }

  private updateOnlineStatus(online: boolean) {
    if (online) {
      this.retryRequestHelper.retryRequests();
    }
    this.onLineSubject.next(online);
  }
}
