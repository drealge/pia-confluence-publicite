import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';
import { FeatureCollection } from 'geojson';
import { ControlPreparation } from '../models/control/controlPreparation';
import { User } from '../models/user';
import 'rxjs-compat/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class ControlService {
  static readonly BASE_URL: String = environment.baseUrlApi;

  constructor(private httpClient: HttpClient) {
  }

  save(control: ControlPreparation): Observable<ControlPreparation> {
    control.geoJson = null;
    return this.httpClient.post<ControlPreparation>(`${ControlService.BASE_URL}/control`, control).pipe(map(this.fixFormatIssues));
  }

  get(id: number): Observable<ControlPreparation> {
    return this.httpClient.get<ControlPreparation>(`${ControlService.BASE_URL}/control/${id}`).pipe(map(this.fixFormatIssues));
  }

  private fixFormatIssues(control: ControlPreparation) {
    control.controlDate = new Date(control.controlDate);
    if (control.statistics.startDate) {
      control.statistics.startDate = new Date(control.statistics.startDate);
    }
    control.geoJson = <FeatureCollection>JSON.parse(<string><unknown>control.geoJson);
    return control;
  }

  findRecentByDepartmentCode(departmentCode: number, limit?: number, beforeDate?: Date): Observable<ControlPreparation[]> {
    const params = {};

    if (limit) {
      Object.assign(params, { countFetch: limit.toString() });
    }
    if (beforeDate) {
      Object.assign(params, { beforeDate: beforeDate.toDateString() });
    }

    return this.httpClient.get<ControlPreparation[]>(`${ControlService.BASE_URL}/control/byDepartment/${departmentCode}`,
      { params: params }).pipe(
        map(controls => {
          return controls.map(this.fixFormatIssues);
        }));
  }

  delete(id: number) {
    return this.httpClient.delete(`${ControlService.BASE_URL}/control/${id}`);
  }

  findActiveByUser(user: User): Observable<ControlPreparation> {
    return this.httpClient.get<ControlPreparation>(`${ControlService.BASE_URL}/control/active/byControllerMail/${user.mail}`)
      .map(this.fixFormatIssues);
  }
}

