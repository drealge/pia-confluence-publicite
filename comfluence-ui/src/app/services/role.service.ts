import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { Role } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class RoleService {
  static readonly BASE_URL: String = environment.baseUrlApi;

  constructor(private httpClient: HttpClient) {
  }

  findAll(): Observable<Role[]> {
    return this.httpClient.get<Role[]>(`${RoleService.BASE_URL}/role`);
  }
}
