import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { AuthenticationService } from './authentication.service';
import { Box2154 } from '../models/box';
import { Coordinates } from '../models/device/properties/coordinates';

export class LayerDefinition {
  id: number;
  name: string;
  url: string;
  departSpecific: boolean;
  needProxy: boolean;
  parameters: {
    map?: string;
    service: string;
    version: string;
    request: string;
    typeName: string;
    outputFormat: string;
    srsName: string;
    bbox?: string;
  };
}

@Injectable({
  providedIn: 'root'
})
export class LayerService {
  static readonly BASE_URL: String = environment.baseUrlApi;
  private layersCachedSubject: BehaviorSubject<boolean>;
  public layersCached: Observable<boolean>;

  constructor(private httpClient: HttpClient, private authenticationService: AuthenticationService) {
    this.layersCachedSubject = new BehaviorSubject<boolean>(false);
    this.layersCached = this.layersCachedSubject.asObservable();
  }

  findAll(box: Box2154): Observable<LayerDefinition[]> {
    const depCode = this.authenticationService.currentUserValue.departement.code;
    // south east : lat = minLat, long = maxLong
    const bbox = box.maxLat.toString() + ',' + box.maxLong.toString() + ',' +
      box.minLat.toString() + ',' + box.minLong.toString();
    // todo ws byBbox
    return this.httpClient.get<LayerDefinition[]>(`${LayerService.BASE_URL}/layer/byDepartement/${depCode}`);
  }

  searchRestrictions(depCode: number, x: number, y: number): Observable<string[]> {
    const params = new HttpParams().set('x', x.toString())
      .set('y', y.toString());
    return this.httpClient.get<string[]>(`${LayerService.BASE_URL}/layer/searchRestriction/${depCode}`, { params: params });
  }
}
