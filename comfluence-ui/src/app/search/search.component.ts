import { Component, Inject, LOCALE_ID, OnDestroy } from '@angular/core';
import { Device } from '../models/device/device';
import { DeviceService } from '../services/device.service';
import 'rxjs-compat/add/operator/first';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { NetworkStatusService } from '../services/network-status.service';
import { Subscription } from 'rxjs';
import { formatDate } from '@angular/common';
import { ReplayDevicesService } from './services/replay-devices.service';

enum SearchType {
  REFERENCE = 'REFERENCE',
  AVERTISER = 'AVERTISER',
  PLACE = 'PLACE'
}

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnDestroy {

  private readonly DEFAULT_RADIO = {
    id: 'reference',
    label: 'Par référence',
    valueSelected: SearchType.REFERENCE,
    labelText: 'la référence',
    click: () => this.currentLabelText = 'la référence'
  };

  searchForm: FormGroup;
  results: Device[] = [];
  searchedBefore = false;
  searched = false;
  isWaitingResponse = false;
  currentLabelText: string = this.DEFAULT_RADIO.labelText;
  online = false;
  private createdSubscriptions: Subscription[] = [];

  radioDefs = [
    this.DEFAULT_RADIO,
    {
      id: 'avertiser',
      label: 'Par publicitaire',
      valueSelected: SearchType.AVERTISER,
      click: () => this.currentLabelText = 'le publicitaire'
    },
    {
      id: 'place',
      label: 'Par commune',
      valueSelected: SearchType.PLACE,
      click: () => this.currentLabelText = 'la commune'
    }
  ];

  dateIsSelected = false;

  constructor(private deviceService: DeviceService,
    private replayDevicesService: ReplayDevicesService,
    private router: Router,
    @Inject(LOCALE_ID) public locale: string,
    private formBuilder: FormBuilder,
    private networkStatusService: NetworkStatusService) {
    const today = Date.now();
    this.searchForm = this.formBuilder.group({
      text: this.formBuilder.control(''),
      type: this.formBuilder.control(this.DEFAULT_RADIO.valueSelected),
      dateMin: this.formBuilder.control(formatDate(today, 'yyyy-MM-dd', this.locale)),
      dateMax: this.formBuilder.control(formatDate(today, 'yyyy-MM-dd', this.locale))
    });
    const subscription = this.networkStatusService.onLine.subscribe(value => this.online = value);
    this.createdSubscriptions.push(subscription);
    this.replayDevicesService.lastResults.first().subscribe({
      next: devices => {
        if (devices && devices.length > 0) {
          this.searchedBefore = true;
          this.results = devices;
        }
      }
    });
  }

  ngOnDestroy(): void {
    this.createdSubscriptions.forEach(subscription => subscription.unsubscribe());
    this.createdSubscriptions = [];
  }

  sendSearch(searchRequest: SearchRequest) {
    this.isWaitingResponse = true;
    this.deviceService.findAll().first().subscribe({
      next: devices => {
        this.searched = true;
        this.results = devices.filter(value => this.buildFilter(searchRequest)(value));
        this.replayDevicesService.updateLastResultsSubject(this.results);
        this.isWaitingResponse = false;
      }
    });
  }

  selectItem(device: Device) {
    this.router.navigate(['/new'], { queryParams: { id: device.id } });
  }

  private buildFilter(searchRequest: SearchRequest): (d: Device) => boolean {
    const regExp = new RegExp(searchRequest.text.toLowerCase());
    let startDate: Date, endDate: Date;
    if (searchRequest.dateMin !== '') {
      startDate = new Date(searchRequest.dateMin);
    }
    if (searchRequest.dateMax !== '') {
      endDate = new Date(searchRequest.dateMax);
      endDate.setDate(endDate.getDate() + 1);
    }
    const filterDate = (device) => (startDate ? startDate <= device.operationDate : true)
      && (endDate ? device.operationDate <= endDate : true);
    switch (searchRequest.type) {
      case SearchType.REFERENCE:
        return function (d: Device) {
          return regExp.test(d.reference.toLowerCase()) && filterDate(d);
        };
      case SearchType.AVERTISER:
        return function (d: Device) {
          return regExp.test(d.description.advertiser.name.toLowerCase()) && filterDate(d);
        };
      case SearchType.PLACE:
        return function (d: Device) {
          return regExp.test(d.address.city.toLowerCase()) && filterDate(d);
        };
    }
  }
}

export class SearchRequest {
  text: string;
  type: string;
  dateMin: string;
  dateMax: string;
}
