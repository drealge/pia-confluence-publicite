import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ControlPreparation } from '../../../models/control/controlPreparation';
import { User } from '../../../models/user';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-control',
  templateUrl: './control.component.html',
  styleUrls: ['./control.component.scss']
})
export class ControlComponent implements OnInit {

  @Input() control: ControlPreparation;
  @Input() controlled: boolean;
  @Output() controlClick: EventEmitter<ControlPreparation> = new EventEmitter();
  @Input() showDetailContent = false;

  controller: User;
  attendant: User;

  constructor(private userService: UserService) {
  }

  ngOnInit(): void {
    const today = new Date();
    today.setSeconds(0);
    today.setMinutes(0);
    today.setHours(0);
    today.setTime(today.getTime() - today.getTimezoneOffset() * 60 * 1000);
    this.userService.findByMail(this.control.controllerMail).subscribe(controller => this.controller = controller);
    if (this.control.attendantMail) {
      this.userService.findByMail(this.control.attendantMail).subscribe(attendant => this.attendant = attendant);
    }
  }

  public formatStatisticsDuration() {
    const hours = Math.floor(this.control.statistics.duration / 60);
    return hours.toString().padStart(2, '0') + 'h' +
      (this.control.statistics.duration - hours * 60).toString().padStart(2, '0');
  }

  formatStatisticsTraveledDistance() {
    return (this.control.statistics.traveledDistance / 1000).toFixed(1);
  }

  sendClickEvent() {
    this.controlClick.emit(this.control);
  }
}
