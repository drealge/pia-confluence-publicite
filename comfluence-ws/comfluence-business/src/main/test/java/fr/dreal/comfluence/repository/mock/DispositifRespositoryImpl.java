package fr.dreal.comfluence.repository.mock;

import fr.dreal.comfluence.business.data.dispositif.DispositifStatus;
import fr.dreal.comfluence.business.data.dispositif.Type;
import fr.dreal.comfluence.business.entities.dispositif.Dispositif;
import fr.dreal.comfluence.repository.DispositifRespository;
import org.springframework.data.domain.Sort;

import java.util.List;

public class DispositifRespositoryImpl extends BaseRespositoryMock<Dispositif, Long> implements DispositifRespository {
	
	@Override
	public Long countByStatus(DispositifStatus status) {
		return this.objects.stream().filter(dispositif -> dispositif.getStatus() == status).count();
	}
	
	@Override
	public Long countByType(Type type) {
		return this.objects.stream().filter(dispositif -> dispositif.getType() == type).count();
	}
	
	@Override
	public List<Dispositif> findAllSortBy(Sort sort) {
		return null;
	}
	
	@Override
	public List<Dispositif> findAllByBox(Double minLat, Double maxLat, Double minLong, Double maxLong) {
		return null;
	}
	
	@Override
	public List<Dispositif> findAllByControlId(Long idControl) {
		return null;
	}
}
