package fr.dreal.comfluence.business.entities.dispositif.properties;

import fr.dreal.comfluence.business.data.dispositif.Category;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter @Setter
public class Size {
	
	/**
	 * identifiant de base de données
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private Double height;
	
	private Double width;
	
	@Enumerated(value = EnumType.ORDINAL)
	private Category category;
	
	private Double deviceGroundDistance;
	
	private Double mastWidth;
	
	private Double protrusionGroundDistance;
}
