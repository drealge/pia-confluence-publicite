package fr.dreal.comfluence.service;

/**
 * @author SopraGroup
 */
public interface CacheServiceForService {
    String TOKEN_CACHE_NAME = "tokenValidity";
    String PARAMCACHE_NAME = "parameters";
    String LAYER_CACHE_NAME = "layers";
    String PARAMCACHE_NAME_MAX_TIME_FOR_OK_DEVICE = "parameters_maxTimeForOkDevice";
    String PARAM_FILTER_CACHE_NAME = "parameterizedFilter";

    void clearAllTokenCache();
    void clearAllTokenCache(String token);
    void clearMaxTimeForOkDevice();
    void clearParamFilter();
}
