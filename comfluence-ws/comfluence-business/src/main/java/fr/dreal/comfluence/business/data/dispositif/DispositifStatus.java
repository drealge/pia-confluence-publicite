package fr.dreal.comfluence.business.data.dispositif;

/**
 * Liste des status possibles pour un dispositif
 */
public enum DispositifStatus {
    COMPLIANT,
    NONCOMPLIANT,
    TODO
}
