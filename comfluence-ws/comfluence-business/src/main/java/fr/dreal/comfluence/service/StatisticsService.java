package fr.dreal.comfluence.service;

import fr.dreal.comfluence.business.data.DispositifFilter;
import fr.dreal.comfluence.business.data.dispositif.DispositifStatus;
import fr.dreal.comfluence.business.data.dispositif.Type;
import fr.dreal.comfluence.business.dto.dispositif.StatisticsDispositifsDTO;
import fr.dreal.comfluence.business.dto.dispositif.StatisticsProceduresDTO;
import fr.dreal.comfluence.business.entities.Control;
import fr.dreal.comfluence.business.entities.Information;
import fr.dreal.comfluence.business.entities.StatisticsControl;
import fr.dreal.comfluence.business.entities.dispositif.Dispositif;
import fr.dreal.comfluence.business.entities.dispositif.properties.Coordinates;
import fr.dreal.comfluence.repository.DispositifRespository;
import fr.dreal.comfluence.repository.InformationRespository;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Implémentation du service permettant la gestion des statistiques sur les dispositifs
 */
@Service
public class StatisticsService {
	
	/**
	 * repository des dispositifs
	 **/
	private final DispositifRespository dispositifRespository;
	private final InformationRespository informationRespository;
	
	public StatisticsService(DispositifRespository dispositifRespository,
			InformationRespository informationRespository) {
		this.dispositifRespository = dispositifRespository;
		this.informationRespository = informationRespository;
	}
	
	public StatisticsDispositifsDTO findDispositifsStatistiques(Long codeDep) {
		StatisticsDispositifsDTO dto = new StatisticsDispositifsDTO();
		Long todo = dispositifRespository.countByStatusAndAddress_CodeDepartement(DispositifStatus.TODO, codeDep);
		Long compliant = dispositifRespository.countByStatusAndAddress_CodeDepartement(DispositifStatus.COMPLIANT,
				codeDep);
		Long nonCompliant = dispositifRespository.countByStatusAndAddress_CodeDepartement(DispositifStatus.NONCOMPLIANT,
				codeDep);
		
		dto.setNonCompliantDevices(nonCompliant);
		dto.setCompliantDevices(compliant);
		dto.setTodoDevices(todo);
		
		Long signboard = dispositifRespository.countByTypeAndAddress_CodeDepartement(Type.SIGNBOARD, codeDep);
		Long preSignboard = dispositifRespository.countByTypeAndAddress_CodeDepartement(Type.PRESIGNBOARD, codeDep);
		Long ad = dispositifRespository.countByTypeAndAddress_CodeDepartement(Type.AD, codeDep);
		
		dto.setSignboardDevices(signboard);
		dto.setPreSignboardDevices(preSignboard);
		dto.setAdDevices(ad);
		
		dto.setLastCall(LocalDate.now());
		dto.setDepartementId(codeDep);
		return dto;
	}
	
	public StatisticsControl updateStatistics(Control control) {
		final List<Dispositif> dispositifs = control.getDispositifs().stream().collect((Collectors.toList()));
		final int controlledDevices = dispositifs.size();
		StatisticsControl entity = initStatisticsControl(control);
		
		if (controlledDevices == 0) {
			return entity;
		}
		
		if (controlledDevices == 1) {
			entity.setDuration(5L); // 5min if one device
			entity.setStartDate(dispositifs.get(0).getOperationDate());
			return entity;
		}
		
		// here we have minimum 2 devices
		double traveledDistance = 0D;
		final Iterator<Dispositif> dispositifIterator = dispositifs.stream().sorted(
				Comparator.comparing(Dispositif::getOperationDate)).iterator();
		Dispositif dispositif = dispositifIterator.next();
		entity.setStartDate(dispositif.getOperationDate());
		while (dispositifIterator.hasNext()) {
			final Coordinates coordinates = dispositif.getCoordinates();
			if (dispositifIterator.hasNext()) {
				final Dispositif nextDispositif = dispositifIterator.next();
				final Coordinates nextDispositifCoordinates = nextDispositif.getCoordinates();
				traveledDistance += distance(coordinates.getLongitude(), coordinates.getLatitude(),
						nextDispositifCoordinates.getLongitude(), nextDispositifCoordinates.getLatitude());
				dispositif = nextDispositif;
				if (!dispositifIterator.hasNext()) {
					entity.setDuration(
							(dispositif.getOperationDate().getTime() - entity.getStartDate().getTime()) / (1000 * 60));
				}
			}
		}
		entity.setTraveledDistance(traveledDistance);
		return entity;
	}
	
	private StatisticsControl initStatisticsControl(Control control) {
		StatisticsControl entity;
		if (control.getStatisticsControl() == null) {
			control.setStatisticsControl(new StatisticsControl());
		}
		entity = control.getStatisticsControl();
		entity.setDuration(0L);
		entity.setControlledDevices(control.getDispositifs().size());
		entity.setTraveledDistance(0D);
		return entity;
	}
	
	/**
	 * Return the distance between 2 points
	 * https://stackoverflow.com/questions/837872/calculate-distance-in-meters-when-you-know-longitude-and-latitude-in-java#837957
	 *
	 * @param lat1
	 * @param lng1
	 * @param lat2
	 * @param lng2
	 * @return
	 */
	private static double distance(double lat1, double lng1, double lat2, double lng2) {
		double earthRadius = 6371000; //meters
		double dLat = Math.toRadians(lat2 - lat1);
		double dLng = Math.toRadians(lng2 - lng1);
		double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(Math.toRadians(lat1)) * Math.cos(
				Math.toRadians(lat2)) * Math.sin(dLng / 2) * Math.sin(dLng / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		return earthRadius * c;
	}
	
	public List<StatisticsProceduresDTO> findProcedureStatistiques(Long codeDep) {
		List<StatisticsProceduresDTO> statisticsProceduresDTOList = new ArrayList<>();
		StatisticsProceduresDTO statisticsProceduresAmiable = findOutdatedProcedureWithFilter(DispositifFilter.AMIABLE,
				codeDep);
		statisticsProceduresDTOList.add(statisticsProceduresAmiable);
		StatisticsProceduresDTO statisticsProceduresAMD = findOutdatedProcedureWithFilter(DispositifFilter.S_ADM,
				codeDep);
		statisticsProceduresDTOList.add(statisticsProceduresAMD);
		
		StatisticsProceduresDTO statisticsProceduresAmende = findOutdatedProcedureWithFilter(DispositifFilter.A_ADM,
				codeDep);
		statisticsProceduresDTOList.add(statisticsProceduresAmende);
		
		StatisticsProceduresDTO statisticsProceduresPv = new StatisticsProceduresDTO();
		statisticsProceduresAmende.setLabel(DispositifFilter.PV.getLabel());
		statisticsProceduresAmende.setFilterId(Collections.singletonList(DispositifFilter.PV.getId()));
		final long count = informationRespository.findAllByFilterAndDepCode(DispositifFilter.PV, codeDep).stream().map(
				Information::getDispositif).map(Dispositif::getId).collect(Collectors.toSet()).size();
		statisticsProceduresAmende.setQuantity(
				((long) Integer.MAX_VALUE > count) ? Math.toIntExact(count) : Integer.MAX_VALUE);
		statisticsProceduresDTOList.add(statisticsProceduresPv);
		
		return statisticsProceduresDTOList;
	}
	
	private StatisticsProceduresDTO findOutdatedProcedureWithFilter(DispositifFilter filter, Long codeDep) {
		StatisticsProceduresDTO statisticsProceduresDTO = new StatisticsProceduresDTO();
		statisticsProceduresDTO.setLabel(filter.getLabel());
		statisticsProceduresDTO.setFilterId(Arrays.asList(filter.getId(), DispositifFilter.HORS_DELAI.getId()));
		List<Information> informations = informationRespository.findAllByFilterAndDepCode(filter, codeDep);
		if (informations != null) {
			Set<Long> idDevice = new HashSet<>();
			informations.stream().filter(information -> {
				if (information.getProcedure().getDelay() != null) {
					LocalDate limitDay = LocalDate.now().plus(-information.getProcedure().getDelay(), ChronoUnit.DAYS);
					return information.getAddedDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().isBefore(
							limitDay);
				}
				return false;
			}).forEach(information -> idDevice.add(information.getDispositif().getId()));
			if (!idDevice.isEmpty()) {
				statisticsProceduresDTO.setQuantity(idDevice.size());
			}
		}
		return statisticsProceduresDTO;
	}
}
