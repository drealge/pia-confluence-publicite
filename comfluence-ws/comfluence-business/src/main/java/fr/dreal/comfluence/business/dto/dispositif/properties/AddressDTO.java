package fr.dreal.comfluence.business.dto.dispositif.properties;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AddressDTO {

    @Getter @Setter
    private Long id;

    @Getter @Setter
    private String number;
    
    @Getter @Setter
    private String street;
    
    @Getter @Setter
    private Integer code;
    
    @Getter @Setter
    private String city;
    
    /** code insee de la commune de l'adresse **/
    @Getter @Setter
    private String inseeCode;
    /**  Code du département **/
    @Getter @Setter
    private Long codeDepartement;
}
