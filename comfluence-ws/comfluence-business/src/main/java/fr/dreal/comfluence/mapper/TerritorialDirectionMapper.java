package fr.dreal.comfluence.mapper;

import fr.dreal.comfluence.business.dto.TerritorialDirectionDto;
import fr.dreal.comfluence.business.entities.TerritorialDirection;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public abstract class TerritorialDirectionMapper {
	public static final TerritorialDirectionMapper INSTANCE = Mappers.getMapper(TerritorialDirectionMapper.class);
	
	public abstract TerritorialDirectionDto entityToDto(TerritorialDirection advertiser);
	
	public abstract TerritorialDirection dtoToEntity(TerritorialDirectionDto advertiser);
}
