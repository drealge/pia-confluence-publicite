package fr.dreal.comfluence.business.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter @Setter
public class TerritorialDirection {
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private String prefecture;
	
	private String service;
	
	private String pole;
	
	private String city;
	
	private String director;
	
	private String direction;
	
	private String phoneNumber;
	
	private String faxNumber;
	
	private String address;
	
	private String signingAuthority;
	
	private String firstNameSigningAuthority;
	
	private String lastNameSigningAuthority;
	
}
