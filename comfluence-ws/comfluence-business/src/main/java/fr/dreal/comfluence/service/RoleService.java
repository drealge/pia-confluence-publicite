package fr.dreal.comfluence.service;

import fr.dreal.comfluence.business.dto.RoleDto;
import fr.dreal.comfluence.mapper.RoleMapper;
import fr.dreal.comfluence.repository.RoleRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author SopraGroup
 */
@Service
public class RoleService {
    public static final String ROLE_ADMIN = "ADMIN";
    public static final String ROLE_USER = "USER";
    private final RoleRepository roleRepository;

    public RoleService(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    public List<RoleDto> findAll() {
        return roleRepository.findAll().stream().map(RoleMapper.INSTANCE::entityToDto).collect(Collectors.toList());
    }
}
