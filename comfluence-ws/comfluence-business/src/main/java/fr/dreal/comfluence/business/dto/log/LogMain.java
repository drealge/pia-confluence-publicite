package fr.dreal.comfluence.business.dto.log;

import lombok.Getter;

/**
 * @author SopraGroup
 */
@Getter
public class LogMain {
    private String utilisateur;
    private String action;

    public LogMain(String utilisateur, String action) {
        this.utilisateur = utilisateur;
        this.action = action;
    }
    protected StringBuilder getStrBuilder() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Utilisateur[").append(utilisateur).append("] - ")
                .append("action[").append(action).append("]");
        return stringBuilder;
    }
    @Override
    public String toString() {
        return getStrBuilder().toString();
    }
}
