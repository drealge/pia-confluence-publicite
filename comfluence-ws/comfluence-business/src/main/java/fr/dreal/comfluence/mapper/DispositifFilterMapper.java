package fr.dreal.comfluence.mapper;

import fr.dreal.comfluence.business.data.DispositifFilter;
import fr.dreal.comfluence.business.dto.dispositif.DispositifFilterDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;


/**
 * Mapper entre les différents objets concernant les tâches.
 */
@Mapper
public abstract class DispositifFilterMapper {
	
	public static final DispositifFilterMapper INSTANCE = Mappers.getMapper(DispositifFilterMapper.class);
	
	public DispositifFilterDTO enumToDto(DispositifFilter dispositifFilter) {
		DispositifFilterDTO dto = new DispositifFilterDTO();
		dto.setId(dispositifFilter.getId());
		dto.setLabel(dispositifFilter.getLabel());
		dto.setNeedReload(dispositifFilter.isNeedReload());
		return dto;
	}
	
	public DispositifFilter dtoToEnum(DispositifFilterDTO dispositifFilterDTO) {
		DispositifFilter result = null;
		for (DispositifFilter dispositifFilter : DispositifFilter.values()) {
			if (dispositifFilter.getId().equals(dispositifFilterDTO.getId())) {
				result = dispositifFilter;
				break;
			}
		}
		return result;
	}
	
}

