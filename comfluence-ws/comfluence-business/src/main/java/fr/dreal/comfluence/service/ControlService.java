package fr.dreal.comfluence.service;

import fr.dreal.comfluence.business.data.dispositif.DispositifStatus;
import fr.dreal.comfluence.business.dto.ControlDto;
import fr.dreal.comfluence.business.entities.Control;
import fr.dreal.comfluence.business.entities.StatisticsControl;
import fr.dreal.comfluence.business.entities.User;
import fr.dreal.comfluence.mapper.CityMapper;
import fr.dreal.comfluence.mapper.StatisticsControlMapper;
import fr.dreal.comfluence.repository.CityRepository;
import fr.dreal.comfluence.repository.ControlRepository;
import fr.dreal.comfluence.repository.GeometryConverterRepository;
import fr.dreal.comfluence.repository.UserRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ControlService {
	
	private static final String CONTROL_DATE_FIELD_NAME = "controlDate";
	
	private ControlRepository controlRepository;
	
	private UserRepository userRepository;
	
	private GeometryConverterRepository geometryConverterRepository;
	
	private CityRepository cityRepository;
	
	@Autowired
	public ControlService(ControlRepository controlRepository, UserRepository userRepository,
			GeometryConverterRepository geometryConverterRepository, CityRepository cityRepository) {
		this.controlRepository = controlRepository;
		this.userRepository = userRepository;
		this.geometryConverterRepository = geometryConverterRepository;
		this.cityRepository = cityRepository;
	}
	
	public ControlDto save(ControlDto controlDto) {
		Control control;
		final Long id = controlDto.getId();
		if (id != null) {
			final Optional<Control> optionalControl = controlRepository.findById(id);
			if (optionalControl.isPresent()) {
				control = optionalControl.get();
			} else {
				return null;
			}
		} else {
			control = new Control();
		}
		mergeInEntity(controlDto, control);
		control = controlRepository.save(control);
		return entityToDto(control);
	}
	
	public Control save(Control control) {
		return controlRepository.save(control);
	}
	
	private void mergeInEntity(ControlDto controlDto, Control control) {
		User controller = userRepository.findFirstByMailAndEnabledTrue(controlDto.getControllerMail());
		control.setController(controller);
		final String attendantMail = controlDto.getAttendantMail();
		User attendant = null;
		if (StringUtils.isNotBlank(attendantMail)) {
			attendant = userRepository.findFirstByMailAndEnabledTrue(attendantMail);
		}
		control.setAttendant(attendant);
		if (control.getDepartement() == null) {
			control.setDepartement(controller.getDepartement());
		}
		
		control.setControlDate(controlDto.getControlDate());
		control.setGeometry(geometryConverterRepository.convertGeoJsonToGeometry(controlDto.getGeoJsonParts()));
		if (control.getStatisticsControl() == null) {
			control.setStatisticsControl(new StatisticsControl());
		}
		control.setCities(cityRepository.findAllCityIntersecting(control.getGeometry()));
	}
	
	public ControlDto findById(Long id) {
		Optional<Control> optionalControl = controlRepository.findById(id);
		return optionalControl.map(this::entityToDto).orElse(null);
	}
	
	private ControlDto entityToDto(Control control) {
		ControlDto controlDto = new ControlDto();
		controlDto.setId(control.getId());
		final User attendant = control.getAttendant();
		controlDto.setControllerMail(control.getController().getMail());
		if (attendant != null) {
			controlDto.setAttendantMail(attendant.getMail());
		}
		controlDto.setControlDate(control.getControlDate());
		String geoJson = geometryConverterRepository.convertGeometryToGeoJson(control.getId());
		controlDto.setGeoJson(geoJson);
		controlDto.setStatistics(StatisticsControlMapper.INSTANCE.entityToDto(control.getStatisticsControl()));
		controlDto.setCities(
				control.getCities().stream().map(CityMapper.INSTANCE::entityToDto).collect(Collectors.toList()));
		
		return controlDto;
	}
	
	private List<ControlDto> findAllByDepartementCodeAndControlDateBefore(Long departmentCode, Date beforeDate) {
		Sort sort = Sort.by(Sort.Direction.DESC, CONTROL_DATE_FIELD_NAME);
		List<Control> result = controlRepository.findAllByDepartementCodeAndControlDateBefore(departmentCode,
				beforeDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate(), sort);
		return result.stream().map(this::entityToDto).collect(Collectors.toList());
	}
	
	private List<ControlDto> findAllByDepartementCodeAndControlDateBefore(Long departmentCode, Date beforeDate,
			Integer countFetch) {
		Pageable pageable = PageRequest.of(0, countFetch, Sort.by(Sort.Direction.DESC, CONTROL_DATE_FIELD_NAME));
		List<Control> result = controlRepository.findAllByDepartementCodeAndControlDateBefore(departmentCode,
				beforeDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate(), pageable);
		return result.stream().map(this::entityToDto).collect(Collectors.toList());
	}
	
	private List<ControlDto> findRecentByDepartmentCode(Long departmentCode, Integer countFetch) {
		Pageable pageable = PageRequest.of(0, countFetch, Sort.by(Sort.Direction.DESC, CONTROL_DATE_FIELD_NAME));
		List<Control> result = controlRepository.findAllByDepartementCode(departmentCode, pageable);
		return result.stream().map(this::entityToDto).collect(Collectors.toList());
	}
	
	private List<ControlDto> findRecentByDepartmentCode(Long departmentCode) {
		List<Control> result = controlRepository.findAllByDepartementCode(departmentCode);
		return result.stream().map(this::entityToDto).collect(Collectors.toList());
	}
	
	public void delete(Long id) {
		controlRepository.deleteById(id);
	}
	
	public List<ControlDto> findByDepartmentCode(Long departmentCode, Date beforeDate, Integer countFetch) {
		List<ControlDto> result;
		if (beforeDate != null) {
			if (countFetch != null) {
				result = findAllByDepartementCodeAndControlDateBefore(departmentCode, beforeDate, countFetch);
			} else {
				result = findAllByDepartementCodeAndControlDateBefore(departmentCode, beforeDate);
			}
		} else {
			if (countFetch != null) {
				result = findRecentByDepartmentCode(departmentCode, countFetch);
			} else {
				result = findRecentByDepartmentCode(departmentCode);
			}
		}
		return result;
	}
	
	public ControlDto findActiveByUserMail(String userMail) {
		List<Control> control = controlRepository.findByControllerMailAndControlDateEquals(userMail, LocalDate.now());
		ControlDto controlDto = null;
		if (control != null && !control.isEmpty()) {
			controlDto = this.entityToDto(control.get(0));
		}
		return controlDto;
	}
	
	public List<Control> findActiveByUserMailAndDate(String userMail, LocalDate date) {
		return controlRepository.findByControllerMailAndControlDateEquals(userMail, date);
	}
	
	public Control createControl(User user) {
		Control newControl = new Control();
		newControl.setController(user);
		newControl.setControlDate(LocalDate.now());
		newControl.setDepartement(user.getDepartement());
		newControl.setDispositifs(new ArrayList<>());
		newControl = controlRepository.save(newControl);
		return newControl;
	}
	
	public List<Control> findAllByDispositifsId(Long id) {
		return controlRepository.findAllByDispositifsId(id);
	}
	
	public List<Control> findAllForLicorneExport(Long departementCode, LocalDate localDateMin,
			LocalDate localDateMax) {
		return controlRepository
				.findAllByDepartementCodeAndControlDateBetweenAndDispositifs_StatusNot(departementCode, localDateMin, localDateMax,
						DispositifStatus.TODO);
	}
}
