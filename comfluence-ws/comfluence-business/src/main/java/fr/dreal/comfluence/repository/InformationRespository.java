package fr.dreal.comfluence.repository;

import fr.dreal.comfluence.business.data.DispositifFilter;
import fr.dreal.comfluence.business.entities.Information;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository pour les objets métier "dispositif"
 */
@Repository
@Component
public interface InformationRespository extends JpaRepository<Information, Long> {

	@Query("SELECT i FROM Information i WHERE i.dispositif.id = :id")
	List<Information> findAllByIdDevice(@Param("id") Long id);

    /**
     * Récupère toutes les procédures pour les filtres données pour un département
     *
     * @param filter list de filtres {@link DispositifFilter}
     * @return liste de {@link Information}
     */
    @Query("select i from Information i join i.procedure p where " +
			"i.closeDate is null and p is not null and " +
            ":filter MEMBER of p.dispositifFilters and i.dispositif.address.codeDepartement = :iddep")
    List<Information> findAllByFilterAndDepCode(@Param("filter")DispositifFilter filter, @Param("iddep") Long iddep);

    /**
     * Trouve toutes les procédures actives pour un dispositif
     * @param idDispositif id du dispositif recherché
     * @return list de {@link Information}
     */
	@Query("select i from Information i join i.procedure p where " +
            "i.closeDate is null and p is not null and " +
            "i.dispositif.id = :idDispositif")
	List<Information> findAllOpenProcByDevice(@Param("idDispositif") Long idDispositif);
}
