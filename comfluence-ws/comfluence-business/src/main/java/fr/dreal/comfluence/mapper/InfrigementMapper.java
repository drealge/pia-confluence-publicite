package fr.dreal.comfluence.mapper;

import fr.dreal.comfluence.business.dto.dispositif.properties.InfrigementDTO;
import fr.dreal.comfluence.business.entities.dispositif.properties.Infrigement;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public abstract class InfrigementMapper {
	
	public static final InfrigementMapper INSTANCE = Mappers.getMapper( InfrigementMapper.class );
	
	public abstract InfrigementDTO entityToDto(Infrigement infrigement);
}
