package fr.dreal.comfluence.business.entities;

import fr.dreal.comfluence.business.data.ChangeType;
import fr.dreal.comfluence.business.entities.dispositif.Dispositif;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter @Setter
public class Information {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private String comment;
	
	private String contentType;
	
	private String originalFileName;
	
	private String path;
	
	private Date addedDate;
	
	@ManyToOne
	private Dispositif dispositif;
	
	@Enumerated(value = EnumType.ORDINAL)
	private ChangeType changeType;
	
	@OneToOne
	private Procedure procedure;
	
	private Integer delay;
 
	private Date closeDate;
}
