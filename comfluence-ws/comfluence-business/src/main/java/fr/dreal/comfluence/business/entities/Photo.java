package fr.dreal.comfluence.business.entities;

import fr.dreal.comfluence.business.data.dispositif.PhotoType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDateTime;

/**
 * Représente une photo.
 * Question : une photo est liée à un dispositif ou à un controle ou les 2 ?
 */
@Entity
public class Photo {
    /**
     * identifiant de base de données
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    private Long id;

    /** identifiant du dispositif **/
    @Getter @Setter
    private Long idDispositif;

    /** nom du fichier original **/
    @Getter @Setter
    private String originalFileName;

    /** date de prise de photo **/
    @Getter @Setter
    private LocalDateTime photoDate;

    /** Signature du fichier, pour s'assurer qu'il n'a pas été modifié **/
    @Getter @Setter
    private String sha256;

    @Getter @Setter
    private String path;

    @Getter @Setter
    private String pathThumb;

    @Getter @Setter
    private String contentType;

    @Getter @Setter
    private PhotoType photoType;

    @Getter @Setter
    private Boolean isArchived;
    
    @Getter @Setter
    private Boolean active;

}
