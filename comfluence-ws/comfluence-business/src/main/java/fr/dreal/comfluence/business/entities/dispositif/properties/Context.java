package fr.dreal.comfluence.business.entities.dispositif.properties;

import fr.dreal.comfluence.business.data.dispositif.Location;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Getter @Setter
public class Context {
	
	/**
	 * identifiant de base de données
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private String town;
	
	private Integer population;
	
	private String unit;

    @ElementCollection(fetch = FetchType.EAGER)
	private Collection<String> environmentalConstraints;
	
	@Enumerated(value = EnumType.ORDINAL)
	private Location location;
}
