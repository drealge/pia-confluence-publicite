package fr.dreal.comfluence.business.dto.log;

import java.util.Date;

/**
 * @author SopraGroup
 */
public interface LoggableAction {
    Long getIdDispositif();
    String getComment();
    Date getDate();
    Type getType();

    enum Type {
        ADD,
        MODIFY,
        REMOVE,
    }
}
