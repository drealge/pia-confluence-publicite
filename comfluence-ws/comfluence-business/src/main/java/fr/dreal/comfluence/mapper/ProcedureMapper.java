package fr.dreal.comfluence.mapper;

import fr.dreal.comfluence.business.data.ProcedureGroupe;
import fr.dreal.comfluence.business.dto.ProcedureDto;
import fr.dreal.comfluence.business.entities.Procedure;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;


/**
 * Mapper entre les différents objets concernant les tâches.
 */
@Mapper(uses = {DispositifFilterMapper.class})
public abstract class ProcedureMapper {
	
	public static final ProcedureMapper INSTANCE = Mappers.getMapper(ProcedureMapper.class);
	
	
	@Mapping(target = "incompatibleProcedures", ignore = true)
    @Mapping(target = "groupeId", source = "groupe", qualifiedByName = "procedureGroupetoId")
	public abstract ProcedureDto entityToDto(Procedure procedure);

	@Named("procedureGroupetoId")
    public static int procedureGroupetoId(ProcedureGroupe procedureGroupe) {
	    int result = 0;
	    if(procedureGroupe != null) {
	        result = procedureGroupe.getId();
        }
	    return result;
    }

    @Named("IdToprocedureGroupe")
    public static ProcedureGroupe IdToprocedureGroupe(int groupeId) {
        ProcedureGroupe result = ProcedureGroupe.GROUPEMENT_A;
	    for(ProcedureGroupe procedureGroupe: ProcedureGroupe.values()) {
	        if(groupeId == procedureGroupe.getId()) {
	            result = procedureGroupe;
	            break;
            }
        }
        return result;
    }

    @Mapping(source = "groupeId", target = "groupe", qualifiedByName = "IdToprocedureGroupe")
	public abstract Procedure dtoToEntity(ProcedureDto procedureDto);
}

