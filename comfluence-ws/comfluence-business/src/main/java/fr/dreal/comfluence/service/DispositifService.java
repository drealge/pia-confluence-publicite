package fr.dreal.comfluence.service;

import fr.dreal.comfluence.business.data.DispositifFilter;
import fr.dreal.comfluence.business.dto.dispositif.DispositifDTO;
import fr.dreal.comfluence.business.dto.dispositif.DispositifFilterDTO;
import fr.dreal.comfluence.business.dto.dispositif.properties.DescriptionDTO;
import fr.dreal.comfluence.business.dto.log.LoggableAction;
import fr.dreal.comfluence.business.dto.photo.PhotoDto;
import fr.dreal.comfluence.business.entities.Control;
import fr.dreal.comfluence.business.entities.Departement;
import fr.dreal.comfluence.business.entities.User;
import fr.dreal.comfluence.business.entities.dispositif.Dispositif;
import fr.dreal.comfluence.externalapi.dto.GeoApiCommune;
import fr.dreal.comfluence.externalapi.ws.GeoApiCommunesWs;
import fr.dreal.comfluence.mapper.DispositifFilterMapper;
import fr.dreal.comfluence.mapper.DispositifMapper;
import fr.dreal.comfluence.repository.DepartementRepository;
import fr.dreal.comfluence.repository.DispositifRespository;
import fr.dreal.comfluence.repository.UserRepository;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Implémentation du service permettant la gestion des statistiques sur les dispositifs
 */
@Service(value = "dispositifService")
public class DispositifService {
	private static final Logger LOGGER = LoggerFactory.getLogger(DispositifService.class);
	private static final DateTimeFormatter refDateFormatter = DateTimeFormatter.ofPattern("yyyyMMdd");
	/**
	 * repository des dispositifs
	 **/
	private final DispositifRespository dispositifRespository;
	private final InformationService informationService;
	private final PhotoService photoService;
	private final FilterService filterService;
	private final LogService logService;
	private final DepartementRepository departementRepository;
	private final ControlService controlService;
	private final UserRepository userRepository;
	private final StatisticsService statisticsService;
	private final GeoApiCommunesWs geoApiCommunesWs;
	private final AdvertiserService advertiserService;
	private final ParameterService parameterService;
	private final FileHelper fileHelper;
	
	public DispositifService(DispositifRespository dispositifRespository, InformationService informationService,
			PhotoService photoService, FilterService filterService, LogService logService,
			DepartementRepository departementRepository, ControlService controlService, UserRepository userRepository,
			StatisticsService statisticsService, GeoApiCommunesWs geoApiCommunesWs, AdvertiserService advertiserService,
			ParameterService parameterService, FileHelper fileHelper) {
		this.dispositifRespository = dispositifRespository;
		this.informationService = informationService;
		this.photoService = photoService;
		this.filterService = filterService;
		this.logService = logService;
		this.departementRepository = departementRepository;
		this.controlService = controlService;
		this.userRepository = userRepository;
		this.statisticsService = statisticsService;
		this.geoApiCommunesWs = geoApiCommunesWs;
		this.advertiserService = advertiserService;
		this.parameterService = parameterService;
		this.fileHelper = fileHelper;
	}
	
	/**
	 * Récupère toutes les dispositifs.
	 *
	 * @return List<DispositifDTO>
	 */
	public List<DispositifDTO> getAllDispostifs(String sortedBy, boolean desc) {
		List<Dispositif> dispositifFound;
		if (StringUtils.isBlank(sortedBy)) {
			dispositifFound = dispositifRespository.findAll();
		} else {
			Sort sort = Sort.by(desc ? Sort.Direction.DESC : Sort.Direction.ASC, sortedBy);
			dispositifFound = dispositifRespository.findAllSortBy(sort);
		}
		
		List<DispositifDTO> dispositifs = new ArrayList<>();
		dispositifFound.forEach(item -> {
			DispositifDTO convertedItm = generateDispDTO(item);
			dispositifs.add(convertedItm);
		});
		return dispositifs;
	}
	
	public List<DispositifDTO> getAllDispositifsForBox(Double minLat, Double maxLat, Double minLong, Double maxLong,
			List<Long> appliedFilters) {
		Date maxDate;
		if (appliedFilters != null && appliedFilters.contains(DispositifFilter.SHOW_ALL.getId())) {
			Calendar tmpCal = Calendar.getInstance();
			tmpCal.setTimeInMillis(0);
			maxDate = tmpCal.getTime();
		} else {
			maxDate = parameterService.findMaxOkDeviceTime();
		}
		
		return dispositifRespository.findAllByBox(minLat, maxLat, minLong, maxLong, maxDate).stream().
				map(this::generateDispDTO).collect(Collectors.toList());
	}
	
	/**
	 * Récupère le dispositif avec l'id correspondant.
	 *
	 * @param id, du dispositif.
	 * @return DispositifDTO
	 */
	public DispositifDTO getDispositifById(Long id) {
		Optional<Dispositif> dispositifFound = dispositifRespository.findById(id);
		if (dispositifFound.isEmpty()) {
			throw new EntityNotFoundException("Didn't find a dispositif with id" + id);
		}
		
		return generateDispDTO(dispositifFound.get());
	}
	
	/**
	 * Sauvegarde le dispositif.
	 *
	 * @param dispositifDto, le dispositif à sauvegarder.
	 * @param userMail
	 * @return DispositifDTO
	 */
	public DispositifDTO saveOrUpdateDispositif(DispositifDTO dispositifDto, String userMail) {
		User user = userRepository.findFirstByMailAndEnabledTrue(userMail);
		if (user == null) {
			throw new EntityNotFoundException("Didn't find a user with mail " + userMail);
		}
		final Long departementCode = user.getDepartement().getCode();
		DescriptionDTO description = dispositifDto.getDescription();
		if (description.getAdvertiser().getId() == null) {
			description
					.setAdvertiser(this.advertiserService.addDepartement(description.getAdvertiser(), departementCode));
		}
		dispositifDto.setOperationDate(new Date());
		final Dispositif entity = DispositifMapper.INSTANCE.dtoToEntity(dispositifDto);
		String comment;
		LoggableAction.Type changeType;
		if (dispositifDto.getId() == null) {
			comment = "Ajout du dipositif";
			changeType = LoggableAction.Type.ADD;
			entity.setReference(generateReference(dispositifDto));
			entity.setInsertedDate(LocalDate.now());
			//recherche du département associé
			entity.getAddress().setCodeDepartement(retrieveDepartement(dispositifDto.getAddress().getInseeCode()));
		} else {
			comment = "";
			changeType = LoggableAction.Type.MODIFY;
		}
		Dispositif dispositif = dispositifRespository.save(entity);
		Optional<Departement> departement = departementRepository.findById(departementCode);
		if (departement.isEmpty()) {
			throw new EntityNotFoundException("Didn't find a departement with code " + departementCode);
		}
		
		LocalDate date = LocalDate
				.from(dispositif.getOperationDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
		List<Control> control = controlService.findActiveByUserMailAndDate(userMail, date);
		
		Control modelControl;
		if (control == null || control.isEmpty()) {
			LOGGER.warn("Pas de préparation de contrôle associée. On va en créer un.");
			modelControl = controlService.createControl(user);
		} else {
			modelControl = control.get(0);
		}
		Collection<Dispositif> dispositifs = modelControl.getDispositifs();
		if (dispositifs != null) {
			final Optional<Dispositif> alreadySeen = dispositifs.stream()
					.filter(device -> device.getId().equals(dispositif.getId())).findFirst();
			alreadySeen.ifPresent(dispositifs::remove);
		} else {
			dispositifs = new ArrayList<>();
		}
		dispositifs.add(dispositif);
		statisticsService.updateStatistics(modelControl);
		controlService.save(modelControl);
		
		logService.registerActionOnDispositif("", changeType, comment, dispositif.getId());
		return DispositifMapper.INSTANCE.entityToDto(dispositif);
	}
	
	private String generateReference(DispositifDTO dispositif) {
		StringBuilder referenceBuilder = new StringBuilder();
		//3 premières lettres de la ville
		if (dispositif.getAddress() != null && dispositif.getAddress().getCity() != null) {
			String cityName = dispositif.getAddress().getCity();
			if (cityName.length() > 3) {
				cityName = cityName.substring(0, 3);
			}
			referenceBuilder.append(cityName.toUpperCase());
		}
		//Date du référencement du dispositif AAAAMMJJ
		LocalDateTime today = LocalDateTime.now();
		referenceBuilder.append(today.format(refDateFormatter));
		//numéro incrémental
		Long nbRegisteredDeviceToday = dispositifRespository.countByInsertedDate(LocalDate.now());
		referenceBuilder.append(String.format("%03d", (nbRegisteredDeviceToday + 1)));
		return referenceBuilder.toString();
	}
	
	private Long retrieveDepartement(String inseeCode) {
		GeoApiCommune geoApiCommune = geoApiCommunesWs
				.commune(inseeCode, Collections.singletonList(GeoApiCommune.NOM_CODE_DEP));
		return geoApiCommune.getCodeDepartement();
	}
	
	private DispositifDTO generateDispDTO(Dispositif dispositif) {
		DispositifDTO dispositifDTO = DispositifMapper.INSTANCE.entityToDto(dispositif);
		List<PhotoDto> photoList = photoService.getPhotosByDispositif(dispositifDTO.getId());
		photoList.sort(Comparator.comparingInt(o -> o.getPhotoType().getOrder()));
		dispositifDTO.setPhotos(photoList);
		if (dispositif.getId() != null) {
			Set<DispositifFilter> filters = filterService.retrieveAllFiltersForDisp(dispositif);
			List<DispositifFilterDTO> filterDTOList = filters.stream().map(DispositifFilterMapper.INSTANCE::enumToDto)
					.collect(Collectors.toList());
			dispositifDTO.setTags(filterDTOList);
		}
		return dispositifDTO;
	}
	
	/**
	 * Supprime un dispositif
	 *
	 * @param id id du dispositf à supprimer
	 */
	public void delete(Long id) {
		Dispositif dispositif = dispositifRespository.getOne(id);
		informationService.delete(dispositif.getInformations());
		List<Control> controls = controlService.findAllByDispositifsId(dispositif.getId());
		controls.forEach(control -> {
			control.getDispositifs().removeIf(disp -> disp.getId().equals(id));
			statisticsService.updateStatistics(control);
			controlService.save(control);
		});
		fileHelper.deleteAllFilesDispositif(dispositif.getId());
		dispositifRespository.deleteById(id);
	}
	
	public List<DispositifFilterDTO> getAvailableFilters() {
		List<DispositifFilterDTO> result = new ArrayList<>();
		
		for (DispositifFilter dispositifFilter : DispositifFilter.values()) {
			DispositifFilterDTO dispositifFilterDTO = DispositifFilterMapper.INSTANCE.enumToDto(dispositifFilter);
			dispositifFilterDTO.setLabel(parameterService.fillNameOfParametrizedFilter(dispositifFilter));
			result.add(dispositifFilterDTO);
		}
		return result;
	}
	
	public DispositifDTO removeDispositif(Long id) {
		Dispositif dispositif = dispositifRespository.getOne(id);
		dispositif.setRemoved(true);
		dispositif.setRemovedDate(new Date());
		dispositif.setOperationDate(new Date());
		dispositifRespository.save(dispositif);
		logService.registerActionOnDispositif("", LoggableAction.Type.REMOVE, "Contre visite - Dispositif déposé",
				dispositif.getId());
		informationService.closeAllProcedure(dispositif.getId());
		return DispositifMapper.INSTANCE.entityToDto(dispositifRespository.getOne(id));
	}
	
	public List<DispositifDTO> findAllByControlId(Long idControl) {
		List<Dispositif> dispositifs = dispositifRespository.findAllByControlId(idControl);
		
		List<DispositifDTO> result = new ArrayList<>();
		dispositifs.forEach(device -> {
			DispositifDTO convertedItm = generateDispDTO(device);
			result.add(convertedItm);
		});
		return result;
	}
	
	
}
