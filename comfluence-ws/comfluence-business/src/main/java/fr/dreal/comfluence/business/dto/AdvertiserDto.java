package fr.dreal.comfluence.business.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter @Setter
public class AdvertiserDto {
	private Long id;
	
	private String name;
}
