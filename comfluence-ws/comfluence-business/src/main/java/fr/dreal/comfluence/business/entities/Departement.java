package fr.dreal.comfluence.business.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * @author SopraGroup
 */
@Entity
@Getter @Setter
public class Departement {
    @Id
    private Long code;
    private String name;
    private Double boundXMax;
    private Double boundYMax;
    private Double boundXMin;
    private Double boundYMin;
    
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private TerritorialDirection territorialDirection;
}
