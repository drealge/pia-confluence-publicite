package fr.dreal.comfluence.business.dto.dispositif.properties;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import fr.dreal.comfluence.business.data.dispositif.Location;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class ContextDTO {
	
	private Long id;
	
	private String town;
	
	private Integer population;
	
	private String unit;
	
	private List<String> constraints;
	
	@Enumerated(value = EnumType.ORDINAL)
	private Location location;
}
