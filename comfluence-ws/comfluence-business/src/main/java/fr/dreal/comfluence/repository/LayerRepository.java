package fr.dreal.comfluence.repository;

import fr.dreal.comfluence.business.entities.Layer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LayerRepository extends JpaRepository<Layer, Long> {
	
	List<Layer> findAllByDepartements_Code(Long code);
    List<Layer> findAllByDepartements_CodeAndService(Long code, String service);
    List<Layer> findAllByService(String service);
	List<Layer> findAllByDepartementsEmpty();
	List<Layer> findAllByDepartements_CodeAndServiceAndUsedForRestrictionTrue(Long code, String service);
    List<Layer> findAllByDepartementsEmptyAndService(String service);
    List<Layer> findAllByDepartementsEmptyAndServiceAndUsedForRestrictionTrue(String service);
}

