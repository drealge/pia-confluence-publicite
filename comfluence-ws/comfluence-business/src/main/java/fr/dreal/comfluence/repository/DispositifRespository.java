package fr.dreal.comfluence.repository;

import fr.dreal.comfluence.business.data.dispositif.DispositifStatus;
import fr.dreal.comfluence.business.data.dispositif.Type;
import fr.dreal.comfluence.business.entities.dispositif.Dispositif;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.BitSet;
import java.util.Date;
import java.util.List;

/**
 * Repository pour les objets métier "dispositif"
 */
@Repository
@Component
public interface DispositifRespository  extends JpaRepository<Dispositif, Long> {
	
	Long countByStatus(DispositifStatus status);
	
	Long countByType(Type type);
    Long countByTypeAndAddress_CodeDepartement(Type type, Long codeDepartement);
    Long countByStatusAndAddress_CodeDepartement(DispositifStatus status, Long codeDepartement);
	
	List<Dispositif> findAllSortBy(Sort sort);

	@Query("select d from Dispositif d where d.status <> fr.dreal.comfluence.business.data.dispositif.DispositifStatus.COMPLIANT" +
            " or (d.status = fr.dreal.comfluence.business.data.dispositif.DispositifStatus.COMPLIANT and d.operationDate >= :date)")
    List<Dispositif> findAllNotOk(@Param("date") Date date);

	@Query("select d from Dispositif d where d.coordinates.latitude >= :minLat and d.coordinates.latitude <= :maxLat and" +
            " d.coordinates.longitude >= :minLong and d.coordinates.longitude <= :maxLong" +
            " and (d.status <> fr.dreal.comfluence.business.data.dispositif.DispositifStatus.COMPLIANT" +
            " or (d.status = fr.dreal.comfluence.business.data.dispositif.DispositifStatus.COMPLIANT and d.operationDate >= :date))")
	List<Dispositif> findAllByBox(@Param("minLat") Double minLat, @Param("maxLat") Double maxLat,
                                  @Param("minLong") Double minLong, @Param("maxLong") Double maxLong,
                                  @Param("date") Date date);
	
	@Query(value = "select c.dispositifs from Control c where c.id = :idControl")
	List<Dispositif> findAllByControlId(@Param("idControl") Long idControl);
	Long countByInsertedDate(LocalDate date);
	
}
