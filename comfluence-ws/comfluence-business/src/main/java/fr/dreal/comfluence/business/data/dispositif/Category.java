package fr.dreal.comfluence.business.data.dispositif;

public enum Category {
    NONE,
    SMALL,
    MEDIUM,
    LARGE,
    XLARGE
}
