package fr.dreal.comfluence.service;

import fr.dreal.comfluence.business.dto.dispositif.properties.InfrigementDTO;
import fr.dreal.comfluence.mapper.InfrigementMapper;
import fr.dreal.comfluence.repository.InfrigementRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class InfrigementService {
	
	private InfrigementRepository infrigementRepository;
	
	public InfrigementService(InfrigementRepository infrigementRepository) {
		this.infrigementRepository = infrigementRepository;
	}
	
	public List<InfrigementDTO> findAll() {
		return infrigementRepository.findAll().stream()
				.map(InfrigementMapper.INSTANCE::entityToDto)
				.collect(Collectors.toList());
	}
}
