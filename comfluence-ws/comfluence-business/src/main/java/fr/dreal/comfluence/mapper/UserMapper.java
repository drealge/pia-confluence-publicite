package fr.dreal.comfluence.mapper;

import fr.dreal.comfluence.business.dto.UserDto;
import fr.dreal.comfluence.business.entities.User;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;


/**
 * Mapper entre les différents objets concernant les tâches.
 */
@Mapper(uses = {DepartementMapper.class, RoleMapper.class})
public abstract class UserMapper {
	
	public static final UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

	public abstract UserDto entityToDto(User user);
	
	public abstract User dtoToEntity(UserDto userDto);
}

