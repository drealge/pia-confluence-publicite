package fr.dreal.comfluence.service;

import fr.dreal.comfluence.business.dto.ProcedureDto;
import fr.dreal.comfluence.business.entities.CourrierTemplate;
import fr.dreal.comfluence.business.entities.Procedure;
import fr.dreal.comfluence.mapper.ProcedureMapper;
import fr.dreal.comfluence.repository.ProcedureRespository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Implémentation du service
 */
@Service(value = "procedureService")
public class ProcedureService {
	
	/**
	 * repository des dispositifs
	 **/
	private final ProcedureRespository procedureRespository;
	
	/**
	 * @param repository repository
	 */
	public ProcedureService(ProcedureRespository repository) {
		this.procedureRespository = repository;
	}
	
	private List<Procedure> findAllLinkedToInfrigementsDevice(Long idDevice) {
		return procedureRespository.findAllLinkedToInfrigementsDevice(idDevice);
	}
	
	private List<Procedure> findAllLinkedToInfrigements() {
		return procedureRespository.findAllLinkedToInfrigements();
	}
	
	public List<ProcedureDto> findAllNext(Long idDevice) {
		List<Procedure> procedures = procedureRespository.findAll();
		List<Procedure> proceduresImplied = findAllLinkedToInfrigements();
		List<Procedure> proceduresImpliedByDevice = findAllLinkedToInfrigementsDevice(idDevice);
		proceduresImplied.forEach(procedureImplied -> {
			if (proceduresImpliedByDevice.stream().noneMatch(
					procedureImpliedByDevice -> procedureImpliedByDevice.getId().equals(procedureImplied.getId()))) {
				Optional<Procedure> procedure = procedures.stream().filter(procedureImpliedByDevice -> procedureImpliedByDevice.getId().equals(procedureImplied.getId())).findFirst();
				procedure.ifPresent(procedures::remove);
			}
		});
		return procedures.stream().map(mProcedure -> {
			final ProcedureDto procedureDto = ProcedureMapper.INSTANCE.entityToDto(mProcedure);
			procedureDto.setIncompatibleProcedures(
					mProcedure.getIncompatibleProcedures().stream().map(ProcedureMapper.INSTANCE::entityToDto)
							.collect(Collectors.toList()));
			return procedureDto;
		}).collect(Collectors.toList());
	}
	
	/**
	 * Récupère la liste des courriers à générer pour une procédure
	 *
	 * @param id id de la procédure
	 * @return liste de {@link CourrierTemplate} qui peut être vide ou nulle
	 */
	public List<CourrierTemplate> findAllAssociatedTemplateById(Long id) {
		return procedureRespository.findAllAssociatedTemplateById(id);
	}

	public ProcedureDto findById(long id) {
	    return procedureRespository.findById(id).map(ProcedureMapper.INSTANCE::entityToDto).orElse(null);
    }
}
