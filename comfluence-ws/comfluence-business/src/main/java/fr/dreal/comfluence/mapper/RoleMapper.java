package fr.dreal.comfluence.mapper;

import fr.dreal.comfluence.business.dto.RoleDto;
import fr.dreal.comfluence.business.entities.Role;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public abstract class RoleMapper {
	public static final RoleMapper INSTANCE = Mappers.getMapper(RoleMapper.class);
	
	public abstract RoleDto entityToDto(Role role);
	public abstract Role dtoToEntity(RoleDto roleDto);
}
