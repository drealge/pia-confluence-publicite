package fr.dreal.comfluence.mapper;

import fr.dreal.comfluence.business.dto.AdvertiserDto;
import fr.dreal.comfluence.business.entities.Advertiser;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public abstract class AdvertiserMapper {
	public static final AdvertiserMapper INSTANCE = Mappers.getMapper(AdvertiserMapper.class);
	
	public abstract AdvertiserDto entityToDto(Advertiser advertiser);
	public abstract Advertiser dtoToEntity(AdvertiserDto advertiser);
}
