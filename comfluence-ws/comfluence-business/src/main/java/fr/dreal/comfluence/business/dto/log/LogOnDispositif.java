package fr.dreal.comfluence.business.dto.log;

import lombok.Getter;

import java.util.Date;

/**
 * @author SopraGroup
 */
@Getter
public class LogOnDispositif extends LogMain implements LoggableAction {
    private Date date;
    private Long idDevice;
    private Type type;
    public LogOnDispositif(String utilisateur, Type type, String action, Long idDevice) {
        super(utilisateur, action);
        this.idDevice = idDevice;
        this.type = type;
        date = new Date();
    }

    @Override
    protected StringBuilder getStrBuilder() {
        return super.getStrBuilder().append(" - dispositif[").append(idDevice).append(']');
    }

    @Override
    public String toString() {
        return getStrBuilder().toString();
    }

    @Override
    public Long getIdDispositif() {
        return idDevice;
    }

    @Override
    public String getComment() {
        return getAction();
    }
}
