package fr.dreal.comfluence.business.entities.dispositif.properties;

import fr.dreal.comfluence.business.entities.Procedure;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter @Setter
public class Infrigement {
    /**
     * identifiant de base de données
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    private String natinfCode;
    
    private String offenseNature;
    
    /** prévu par **/
    private String codeArticle;
    
    /** réprimandé par **/
    private String codeArticleReprimand;
    
    @OneToOne(fetch = FetchType.LAZY)
    private Procedure impliedProcedure; // for now, there is only "Amende Administative" for only some NATINF
    
    
}
