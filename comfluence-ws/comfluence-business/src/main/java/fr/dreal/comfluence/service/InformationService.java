package fr.dreal.comfluence.service;

import fr.dreal.comfluence.business.dto.InformationDto;
import fr.dreal.comfluence.business.dto.log.LoggableAction;
import fr.dreal.comfluence.business.entities.Information;
import fr.dreal.comfluence.business.entities.dispositif.Dispositif;
import fr.dreal.comfluence.mapper.InformationMapper;
import fr.dreal.comfluence.repository.DispositifRespository;
import fr.dreal.comfluence.repository.InformationRespository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Implémentation du service
 */
@Service(value = "informationService")
public class InformationService {
	
	/**
	 * Permet l'utilisation de logBack à travers le LOGGER.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(InformationService.class);

	/**
	 * repository des dispositifs
	 **/
	private final DispositifRespository dispositifRespository;
	private final LogService logService;
	private final InformationRespository informationRespository;

	private static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat("dd.MM.yyyy");
	
	/**
	 * @param informationRespository repository
	 */
	public InformationService(InformationRespository informationRespository,
			DispositifRespository dispositifRespository, LogService logService) {
		this.dispositifRespository = dispositifRespository;
		this.informationRespository = informationRespository;
		this.logService = logService;
	}
	
	public Information save(Information information, Long idDevice) {
        final Dispositif dispositif = dispositifRespository.getOne(idDevice);
        information.setDispositif(dispositif);
        information = informationRespository.save(information);
        return information;
	}
	
	
	public InformationDto save(InformationDto informationDto) {
		Information information = InformationMapper.INSTANCE.dtoToEntity(informationDto);
		information = save(information, informationDto.getIdDevice());
		InformationDto informationDtoResponse = InformationMapper.INSTANCE.entityToDto(information);
		informationDtoResponse.setIdDevice(information.getDispositif().getId());
		return informationDtoResponse;
	}
	
	public List<InformationDto> findAllByDevice(Long id) {
		final List<Information> informations = informationRespository.findAllByIdDevice(id);
		if (informations == null || informations.isEmpty()) {
			return Collections.emptyList();
		}
		return informations.stream().map(information -> {
			final InformationDto informationDto = InformationMapper.INSTANCE.entityToDto(information);
			informationDto.setIdDevice(information.getDispositif().getId());
			return informationDto;
		}).collect(Collectors.toList());
	}
	
	public Information getEntity(Long id) {
		return informationRespository.getOne(id);
	}
	
	public Boolean closeProcedure(Long idDevice, Long idDoc, Date closeDate, Boolean sansSuite) {
		Boolean result = Boolean.FALSE;
		Information information = informationRespository.getOne(idDoc);
	    if(information.getDispositif() != null && information.getDispositif().getId().equals(idDevice) && information.getProcedure() != null) {
			information.setCloseDate(Objects.requireNonNullElseGet(closeDate, Date::new));
			informationRespository.save(information);
			if(sansSuite != null && sansSuite) {
                logService.registerActionOnProcedure("", LoggableAction.Type.MODIFY,
                        "La procédure " + information.getProcedure().getLabel() + " a été classée sans suite.", idDevice, idDoc);
            } else if(Boolean.TRUE.equals(information.getProcedure().getIsPv()) && closeDate != null) {
                logService.registerActionOnProcedure("", LoggableAction.Type.MODIFY,
                        "PV fait et clos le " + DATE_FORMATTER.format(closeDate), idDevice, idDoc);
            } else {
                logService.registerActionOnProcedure("", LoggableAction.Type.MODIFY,
                        "La procédure " + information.getProcedure().getLabel() + " est close.", idDevice, idDoc);
            }
			result = Boolean.TRUE;
		} else {
			LOGGER.warn("La procédure {} pour le dispositif {} n'a pas pu être close", idDoc, idDevice);
		}
		return result;
	}
	
	public void closeAllProcedure(Long idDevice) {
		List<Information> procedureList = informationRespository.findAllOpenProcByDevice(idDevice);
		if (procedureList != null && !procedureList.isEmpty()) {
			for (Information procedure : procedureList) {
				procedure.setCloseDate(new Date());
				logService.registerActionOnProcedure("", LoggableAction.Type.MODIFY,
						"La procédure " + procedure.getProcedure().getLabel() + " est close.", idDevice,
						procedure.getId());
			}
			informationRespository.saveAll(procedureList);
		}
	}
	
	public void delete(Collection<Information> informations) {
		informationRespository.deleteAll(informations);
	}
	
	public InformationDto update(Long id, InformationDto informationDto) {
		Information information = this.informationRespository.getOne(id);
		if (informationDto.getDelay() != null) {
			information.setDelay(informationDto.getDelay());
			information = this.informationRespository.save(information);
		}
		return InformationMapper.INSTANCE.entityToDto(information);
	}
	
	public InformationDto get(Long id) {
		return InformationMapper.INSTANCE.entityToDto(this.informationRespository.getOne(id));
	}

	public void remove(Long id) {
	    this.informationRespository.deleteById(id);
    }

    public void update(Information information) {
	    this.informationRespository.save(information);
    }
}
