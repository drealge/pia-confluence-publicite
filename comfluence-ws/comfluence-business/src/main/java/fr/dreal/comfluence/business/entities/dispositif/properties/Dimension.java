package fr.dreal.comfluence.business.entities.dispositif.properties;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
public class Dimension {

    /**
     * identifiant de base de données
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter @Setter
    private Long id;

    @Getter @Setter
    private Boolean isEstimated;

    @Getter @Setter
    @OneToOne(cascade=CascadeType.ALL)
    private Size size;

}
