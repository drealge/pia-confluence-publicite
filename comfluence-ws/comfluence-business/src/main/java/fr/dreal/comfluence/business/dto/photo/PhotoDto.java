package fr.dreal.comfluence.business.dto.photo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import fr.dreal.comfluence.business.data.dispositif.PhotoType;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PhotoDto{
    /**
     * identifiant de base de données
     */
    @Getter @Setter
    private Long id;

    /** identifiant du dispositif **/
    @Getter @Setter
    private Long idDispositif;

    /** nom du fichier sur le filesystème, par defaut id photo + nom du fichier original **/
    @Getter @Setter
    private String uniqueFileName;

    /** nom du fichier original **/
    @Getter @Setter
    private String originalFileName;

    /** date de prise de photo **/
    @Getter @Setter
    private LocalDateTime photoDate;

    @Getter @Setter
    private PhotoType photoType;
    
    @Getter @Setter
    private Boolean active;
    
    @Getter @Setter
    private Boolean imageLinked;
}
