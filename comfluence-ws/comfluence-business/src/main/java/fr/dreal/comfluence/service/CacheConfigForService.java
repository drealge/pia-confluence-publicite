package fr.dreal.comfluence.service;

import org.springframework.boot.autoconfigure.cache.CacheManagerCustomizer;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 * @author SopraGroup
 */
@Component
public class CacheConfigForService implements CacheManagerCustomizer<ConcurrentMapCacheManager>, CacheServiceForService {

    @Override
    public void customize(ConcurrentMapCacheManager cacheManager) {
        cacheManager.setCacheNames(Arrays.asList(TOKEN_CACHE_NAME, PARAM_FILTER_CACHE_NAME, PARAMCACHE_NAME,
                PARAMCACHE_NAME_MAX_TIME_FOR_OK_DEVICE, LAYER_CACHE_NAME));
    }

    @Override
    @Scheduled(cron = "0 0 2 * * *") //tous les jours à 2h00
    @CacheEvict(value = {TOKEN_CACHE_NAME}, allEntries = true)
    public void clearAllTokenCache() {
    }
    @Override
    @CacheEvict(value = {TOKEN_CACHE_NAME})
    public void clearAllTokenCache(String token) {
    }

    @Scheduled(cron = "0 1 2 * * *") //tous les jours à 2h01
    @Override
    @CacheEvict(value = {PARAMCACHE_NAME_MAX_TIME_FOR_OK_DEVICE})
    public void clearMaxTimeForOkDevice() {
    }

    @Scheduled(cron = "0 1 2 * * *") //tous les jours à 2h01
    @Override
    @CacheEvict(value = {PARAM_FILTER_CACHE_NAME}, allEntries = true)
    public void clearParamFilter() {
    }
}
