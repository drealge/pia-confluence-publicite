package fr.dreal.comfluence.document.service;


import fr.dreal.comfluence.business.data.dispositif.PhotoType;
import fr.dreal.comfluence.business.dto.ProcedureDto;
import fr.dreal.comfluence.business.dto.TerritorialDirectionDto;
import fr.dreal.comfluence.business.dto.UserDto;
import fr.dreal.comfluence.business.dto.dispositif.DispositifDTO;
import fr.dreal.comfluence.business.dto.photo.PhotoDto;
import fr.dreal.comfluence.business.entities.CourrierTemplate;
import fr.dreal.comfluence.business.entities.Photo;
import fr.dreal.comfluence.document.converter.DispositifMergeField;
import fr.dreal.comfluence.document.converter.ProcedureMergeField;
import fr.dreal.comfluence.document.converter.TerritorialDirectionMergeField;
import fr.dreal.comfluence.document.converter.UserMergeField;
import fr.dreal.comfluence.document.dto.FileWrapper;
import fr.dreal.comfluence.document.handler.NullValueInsertionEventHandler;
import fr.dreal.comfluence.service.DepartementService;
import fr.dreal.comfluence.service.PhotoService;
import fr.dreal.comfluence.service.ProcedureService;
import fr.dreal.comfluence.service.UserService;
import fr.opensagres.xdocreport.converter.ConverterTypeTo;
import fr.opensagres.xdocreport.converter.Options;
import fr.opensagres.xdocreport.core.XDocReportException;
import fr.opensagres.xdocreport.core.document.DocumentKind;
import fr.opensagres.xdocreport.document.IXDocReport;
import fr.opensagres.xdocreport.document.images.FileImageProvider;
import fr.opensagres.xdocreport.document.images.IImageProvider;
import fr.opensagres.xdocreport.document.registry.XDocReportRegistry;
import fr.opensagres.xdocreport.template.IContext;
import fr.opensagres.xdocreport.template.TemplateEngineKind;
import fr.opensagres.xdocreport.template.formatter.FieldsMetadata;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.event.EventCartridge;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;


@Service
public class DocumentService {
	/**
	 * Logger
	 **/
	private static final Logger LOGGER = LoggerFactory.getLogger(DocumentService.class);
	
	/**
	 * format de date par défaut
	 **/
	private static final SimpleDateFormat SDF = new SimpleDateFormat("dd MMMM yyyy", Locale.FRANCE);
	private final SimpleDateFormat horodate = new SimpleDateFormat("yyyyMMddHHmmss");
	/**
	 * chemin de sortie du fichier généré
	 **/
	private Path outputPath;
	
	private Path templateDirectoryPath;
	
	private final ProcedureService procedureService;
	private final DepartementService departementService;
	private final PhotoService photoService;
	private final UserService userService;
	
	
	/**
	 * Constructeur
	 *
	 * @param outputDir fichier de sortie
	 */
	public DocumentService(@Value("${comfluence.document.generation.dir:/tmp}") String outputDir,
                           @Value("${comfluence.template.dir:/tmp}") String templateDir, ProcedureService procedureService,
                           DepartementService departementService, PhotoService photoService, UserService userService) {
		this.outputPath = Path.of(outputDir);
		this.templateDirectoryPath = Path.of(templateDir);
		this.procedureService = procedureService;
		this.departementService = departementService;
		this.photoService = photoService;
		this.userService = userService;
	}
	
	
	public FileWrapper generateDocumentFromTemplate(Long idInformation, Long idProcedure, DispositifDTO dispositif, String userLogin) {
		
		List<CourrierTemplate> courrierTemplateList = procedureService.findAllAssociatedTemplateById(idProcedure);
		ProcedureDto procedureDto = procedureService.findById(idProcedure);
		FileWrapper result = null;
		if (courrierTemplateList != null && procedureDto != null) {
			for (CourrierTemplate courrierTemplate : courrierTemplateList) {
				result = generateDocumentFromTemplate(idInformation, courrierTemplate, dispositif, procedureDto, userLogin);
			}
		}
		return result;
	}
	
	private FileWrapper generateDocumentFromTemplate(Long idInformation, CourrierTemplate courrierTemplate,
			DispositifDTO dispositif, ProcedureDto procedureDto, String userLogin) {
		// charge le template
		String templateFile = courrierTemplate.getTemplateName();
		
		// Si le chemin de sortie n'existe pas, on le créé
		StringBuilder fileNameBuilder = new StringBuilder(outputPath.toString()).append(File.separator).append(
				dispositif.getId()).append(File.separator).append(idInformation);
		File folder = new File(fileNameBuilder.toString());
		if (!folder.exists()) {
			folder.mkdirs();
		}
		fileNameBuilder.append(File.separator).append(buildProcedureName(courrierTemplate));
		File outputFile = new File(fileNameBuilder.toString());
		try (InputStream in = templateDirectoryPath.resolve(
				templateFile).toUri().toURL().openStream(); OutputStream out = new FileOutputStream(outputFile)) {
			
			IXDocReport report = XDocReportRegistry.getRegistry().loadReport(in, TemplateEngineKind.Velocity);
			IContext context = report.createContext();
			initCustomEventHandlers((VelocityContext) context);
			UserDto user = userService.findByMail(userLogin);
			if(user != null) {
			    fillContext(context, user);
                TerritorialDirectionDto territorialDirection = departementService.getTerritorialDirection(user.getDepartement().getCode());
                if(territorialDirection != null) {
                    fillContext(context, territorialDirection);
                }
            } else {
			    LOGGER.warn("L'utilisateur {} non trouvé", userLogin);
            }
			fillContext(context, procedureDto);
			fillContext(context, dispositif);
			final Optional<PhotoDto> photoNear = dispositif.getPhotos().stream().filter(
					photo -> PhotoType.NEAR.equals(photo.getPhotoType())).findFirst();
			photoNear.ifPresent(photoDto -> addDispositifPhoto(report, context, photoDto));
			context.put("date", SDF.format(new Date()));
			report.process(context, out);
		} catch (IOException | XDocReportException e) {
			LOGGER.error("Impossible de générer le fichier courrier dû à une erreur d'accès au fichier.", e);
		}
		
		return new FileWrapper(outputFile, courrierTemplate.getGeneratedName());
	}
	
	private void initCustomEventHandlers(VelocityContext context) {
		EventCartridge eventCartridge = new EventCartridge();
		eventCartridge.addEventHandler(new NullValueInsertionEventHandler());
		eventCartridge.attachToContext(context);
	}
	
	private void addDispositifPhoto(IXDocReport report, IContext context, PhotoDto photoDto) {
		FieldsMetadata metadata = report.getFieldsMetadata();
		if(metadata == null) {
			metadata = new FieldsMetadata();
		}
		final String photo_dispositif = "photo_dispositif_proche";
		metadata.addFieldAsImage(photo_dispositif);
		report.setFieldsMetadata(metadata);
		Photo photo = photoService.getPhoto(photoDto.getId());
		IImageProvider image = null;
		try {
			final File tempFile = File.createTempFile(photo.getPath(), getType(photo.getContentType()));
			tempFile.deleteOnExit();
			Files.copy(Paths.get(photo.getPath()), tempFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
			image = new FileImageProvider(tempFile);
		} catch (IOException e) {
			LOGGER.error("Exception when adding image to file.", e);
		}
		context.put(photo_dispositif, image);
	}
	
	private String getType(String contentType) {
		String result;
		switch (contentType){
			case "image/png":
				result = ".png";
				break;
			default:
				result = ".png";
		}
		return result;
	}
	
	private String buildProcedureName(CourrierTemplate courrierTemplate) {
		StringBuilder fileNameBuilder = new StringBuilder(horodate.format(new Date())).append('_').append(
				courrierTemplate.getGeneratedName());
		return fileNameBuilder.toString();
	}
	
	public FileWrapper updateProcDocument(String oldFilePath, CourrierTemplate courrierTemplate,
			MultipartFile multipartFile) {
		File dirFile = new File(oldFilePath).getParentFile();
		if (!dirFile.exists()) {
			dirFile.mkdirs();
		}
		FileWrapper result = null;
		File fileToSave = new File(dirFile.getAbsolutePath() + File.separator + buildProcedureName(courrierTemplate));
		try {
			multipartFile.transferTo(fileToSave);
			result = new FileWrapper(fileToSave, courrierTemplate.getGeneratedName());
		} catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
		}
		
		return result;
	}
	
	public byte[] convertDocumentToPdf(byte[] document) {
		IXDocReport xdocGenerator;
		try (final ByteArrayInputStream sourceStream = new ByteArrayInputStream(
				document); ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
			xdocGenerator = XDocReportRegistry.getRegistry().loadReport(sourceStream, TemplateEngineKind.Velocity);
			Options options = Options.getFrom(DocumentKind.ODT).to(ConverterTypeTo.PDF);
			IContext context = xdocGenerator.createContext();
			
			//Merge Java model with the ODT and convert it to PDF
			xdocGenerator.convert(context, options, outputStream);
			return outputStream.toByteArray();
		} catch (IOException | XDocReportException e) {
			LOGGER.error("Impossible de convertir le fichier courrier dû à une erreur d'accès au fichier.", e);
		}
		return null;
	}
	
	
	/**
	 * Rempli le contexte avec les éléments connus
	 *
	 * @param dispositif le dispositif concerné
	 */
	private void fillContext(IContext context, DispositifDTO dispositif) {
		new DispositifMergeField().getRules().forEach((key, function) -> {
		    if(function.getKey().apply(dispositif)) {
                context.put(key, function.getValue().apply(dispositif));
            }

		});
	}
    /**
     * Rempli le contexte avec les éléments connus d'une procédure
     *
     * @param procedureDto la procédure concernée
     */
    private void fillContext(IContext context, ProcedureDto procedureDto) {
        new ProcedureMergeField().getRules().forEach((key, function) -> {
            if(function.getKey().apply(procedureDto)) {
                context.put(key, function.getValue().apply(procedureDto));
            }

        });
    }
	
	/**
	 * Rempli le contexte avec les éléments connus
	 */
	private void fillContext(IContext context, TerritorialDirectionDto territorialDirectionDto) {
		new TerritorialDirectionMergeField().getRules().forEach((key, function) -> {
            if(function.getKey().apply(territorialDirectionDto)) {
                context.put(key, function.getValue().apply(territorialDirectionDto));
            }
		});
	}

    /**
     * Rempli le contexte avec les éléments connus d'un utilisateur
     */
    private void fillContext(IContext context, UserDto userDto) {
        new UserMergeField().getRules().forEach((key, function) -> {
            if(function.getKey().apply(userDto)) {
                context.put(key, function.getValue().apply(userDto));
            }
        });
    }
}
