package fr.dreal.comfluence.document.converter;

public interface EnumConverter<T> extends Converter<T, String> {}
