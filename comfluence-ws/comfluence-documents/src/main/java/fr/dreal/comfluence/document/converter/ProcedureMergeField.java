package fr.dreal.comfluence.document.converter;

import fr.dreal.comfluence.business.dto.ProcedureDto;
import org.apache.commons.lang3.tuple.Pair;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class ProcedureMergeField extends MergeField<ProcedureDto> {
	
	@Override
	protected Map<String, Pair<Function<ProcedureDto, Boolean>,Function<ProcedureDto, Object>>> buildRules() {
		Map<String, Pair<Function<ProcedureDto, Boolean>,Function<ProcedureDto, Object>>> rules = new HashMap<>();
        rules.put("delai_courrier", buildUncheckPair(procedureDto -> procedureDto.getDelay() != null ? procedureDto.getDelay():""));
		return rules;
	}
}
