package fr.dreal.comfluence.document.converter;

import fr.dreal.comfluence.business.data.dispositif.Location;

public class LocationConverter implements EnumConverter<Location> {
	@Override
	public String convert(Location toConvert) {
		String result = null;
		if(toConvert != null) {
            switch (toConvert) {
                case AGGLOMERATION:
                    result = "en agglomération";
                    break;
                case COUNTRYSIDE:
                    result = "hors agglomération";
                    break;
                case UNDEFINED_LOCATION:
                    result = "agglomération : indéfini";
                    break;
            }
        }
		return result;
	}
}
