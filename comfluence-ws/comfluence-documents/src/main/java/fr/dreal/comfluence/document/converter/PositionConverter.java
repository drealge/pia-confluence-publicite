package fr.dreal.comfluence.document.converter;

import fr.dreal.comfluence.business.data.dispositif.Position;

public class PositionConverter implements EnumConverter<Position> {
	@Override
	public String convert(Position toConvert) {
		String result = null;
		if(toConvert != null) {
            switch (toConvert) {
                case NUMERIC:
                    result = "Numérique";
                    break;
                case BRIGHT:
                    result = "Lumineux";
                    break;
                case FACADE:
                    result = "Sur façade";
                    break;
                case BLIND_WALL:
                    result = "Sur mur aveugle";
                    break;
                case NOT_BLIND_WALL:
                    result = "Sur mur non aveugle";
                    break;
                case CEMETERY:
                    result = "Sur mur de cimetière";
                    break;
                case PUBLIC_GARDEN:
                    result = "Sur mur de jardin public";
                    break;
                case BLIND_FENCE:
                    result = "Sur clôture aveugle";
                    break;
                case NOT_BLIND_FENCE:
                    result = "Sur clôture non aveugle";
                    break;
                case ROOF:
                    result = "Sur toiture";
                    break;
                case TREE:
                    result = "Sur arbre";
                    break;
                case POLE:
                    result = "Sur poteau";
                    break;
                case PUBLIC_FURNITURE:
                    result = "Sur équipement public";
                    break;
                case URBAN_FURNITURE:
                    result = "Sur mobilier urbain";
                    break;
                case TARPAULIN:
                    result = "Sur bâche";
                    break;
                case VEHICULE:
                    result = "Sur véhicule terrestre";
                    break;
                case PLANTATION:
                    result = "Sur plantation";
                    break;
            }
        }
		return result;
	}
}
