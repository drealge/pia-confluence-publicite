package fr.dreal.comfluence.document.handler;

import org.apache.velocity.app.event.ReferenceInsertionEventHandler;

public class NullValueInsertionEventHandler implements ReferenceInsertionEventHandler {
	
	@Override
	public Object referenceInsert(String string, Object object) {
		if (object == null) {
			return String.format("&lt;N.R : %s&gt;", string.toUpperCase());
		}
		return object;
	}
}
