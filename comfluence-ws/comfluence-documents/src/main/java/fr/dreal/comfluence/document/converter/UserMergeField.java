package fr.dreal.comfluence.document.converter;

import fr.dreal.comfluence.business.dto.UserDto;
import org.apache.commons.lang3.tuple.Pair;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class UserMergeField extends MergeField<UserDto> {
	
	@Override
	protected Map<String, Pair<Function<UserDto, Boolean>,Function<UserDto, Object>>> buildRules() {
		Map<String, Pair<Function<UserDto, Boolean>,Function<UserDto, Object>>> rules = new HashMap<>();
		rules.put("nom", buildUncheckPair(UserDto::getLastName) );
        rules.put("prenom", buildUncheckPair(UserDto::getLastName) );
		rules.put("mail", buildUncheckPair(UserDto::getMail));
		return rules;
	}
}
