package fr.dreal.comfluence.ws;

import fr.dreal.comfluence.business.dto.dispositif.DispositifDTO;
import fr.dreal.comfluence.business.dto.dispositif.DispositifFilterDTO;
import fr.dreal.comfluence.service.DispositifService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/dispositif")
@Api(value = "Gestion des dispositifs et statistiques pour l'application Comfluence")
public class DispositifController {


	/**
	 * Permet l'utilisation de logBack à travers le LOGGER.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(DispositifController.class);


	private final DispositifService dispositifService;

	/**
	 * Initialisation du user service.
	 *
	 * @param dispositifService, service de gestion de dispositif.
	 */
	public DispositifController(DispositifService dispositifService) {
		this.dispositifService = dispositifService;
	}


	/**
	 * Permet la récupération de tous les dispositifs.
	 * En cas d'échec renvoit une liste vide.
	 *
	 * @return ResponseEntity<List < DispositifDTO>>
	 */
	@ApiOperation(value = "Retourne tout les dispositifs", response = DispositifDTO.class, responseContainer = "List")
	@GetMapping(value = "")
	public ResponseEntity<List<DispositifDTO>> getAllDispositifs(@RequestParam(required = false) String sortedBy,
			@RequestParam(required = false) boolean direction) {
		List<DispositifDTO> dispositifs = dispositifService.getAllDispostifs(sortedBy, direction);
		String logInfo = String.format("Users List : %1$s.", dispositifs.toString());
		LOGGER.info(logInfo);
		return ResponseEntity.ok(dispositifs);
	}


	/**
	 * Permet la récupération de tous les dispositifs.
	 * En cas d'échec renvoit une liste vide.
	 *
	 * @return ResponseEntity<List < DispositifDTO>>
	 */
	@ApiOperation(value = "Retourne tout les dispositifs associés à une préparation de contrôle", response = DispositifDTO.class, responseContainer = "List")
	@GetMapping(value = "/byControl/{idControl}")
	public ResponseEntity<List<DispositifDTO>> findAllByControlId(@PathVariable(value = "idControl") Long idControl) {
		List<DispositifDTO> dispositifs = dispositifService.findAllByControlId(idControl);
		String logInfo = String.format("Users List : %1$s.", dispositifs.toString());
		LOGGER.info(logInfo);
		return ResponseEntity.ok(dispositifs);
	}
    /**
     * Permet la récupération de tous les dispositifs pour une bbox donnée.
     * Et une liste de filtre.
     * En cas d'échec renvoit une liste vide.
     *
     * @return ResponseEntity<List<DispositifDTO>>
     */
    @ApiOperation(value = "Retourne tout les dispositifs pour une bbox donnée.\n" +
            "Et une liste de filtre." +
            "En cas d'échec renvoit une liste vide.", response = DispositifDTO.class, responseContainer = "List")
    @GetMapping(value = "/box")
    public ResponseEntity<List<DispositifDTO>> getAllDispositifsForBox(
            @RequestParam Double minLat,
            @RequestParam Double maxLat,
            @RequestParam Double minLong,
            @RequestParam Double maxLong,
            @RequestParam(required = false) List<Long> appliedFilters) {
        List<DispositifDTO> dispositifIdList = dispositifService.getAllDispositifsForBox(minLat, maxLat, minLong, maxLong, appliedFilters);
        return new ResponseEntity<>(dispositifIdList, HttpStatus.OK);
    }

	/**
	 * Permet la récupération d'un seul dispositif à partir de son id.
	 * En cas d'échec renvoit une liste vide.
	 *
	 * @param id, id du dispositif.
	 * @return ResponseEntity<ResponseUserDTO>
	 */
	@ApiOperation(value = "Retourne un dispositif avec l'id correspondant", response = DispositifDTO.class)
	@GetMapping(value = "/{id}")
	public ResponseEntity<DispositifDTO> getDispositif(@PathVariable(value = "id") Long id) {
		DispositifDTO dispositif = dispositifService.getDispositifById(id);
		String logInfo = String.format("Dispositif Found By Id : %1$s.", dispositif.toString());
		LOGGER.info(logInfo);
		return ResponseEntity.ok(dispositif);
	}

	/**
	 * Permet la sauvegarde d'un dispositif dans la base de données.
	 *
	 * @param dispositifDTO, le dispositif à sauvegarder.
	 * @return ResponseEntity<ResponseUserDTO>
	 */
	@ApiOperation(value = "Sauvegarde un dispositif", response = DispositifDTO.class)
	@PostMapping("")
	public ResponseEntity<DispositifDTO> saveDispositif(@RequestBody DispositifDTO dispositifDTO) {
		String userMail = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		DispositifDTO dispositifInserted = dispositifService.saveOrUpdateDispositif(dispositifDTO, userMail);
		String logInfo = String.format("Dispositif Saved : %1$s.", dispositifInserted.toString());
		LOGGER.info(logInfo);
		return ResponseEntity.status(HttpStatus.CREATED).body(dispositifInserted);
	}

	/**
	 * Permet la modification d'un dispositif dans la base de données.
	 * En cas d'erreur sur l'id, une nouveau dispositif est créé.
	 *
	 * @param id,            du dispositif à modifier.
	 * @param dispositifDTO, dispositif à modifier.
	 * @return ResponseEntity<ResponseUserDTO>
	 */
	@PutMapping(value = "/{id}")
	@ApiOperation(value = "Mets à jour un dispositif", response = DispositifDTO.class)
	public ResponseEntity<DispositifDTO> updateDispositif(@PathVariable(value = "id") Long id,
			@RequestBody DispositifDTO dispositifDTO) {
		String userMail = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		DispositifDTO updatedDispositif = dispositifService.saveOrUpdateDispositif(dispositifDTO, userMail);
		return ResponseEntity.ok(updatedDispositif);
	}


	/**
	 * Supprime de la bdd un dispositif
	 *
	 * @param id id du dispositif
	 * @return 204
	 */
	@DeleteMapping(value = "/{id}")
	@ApiOperation(value = "Permet de supprimer un dispositif")
	public ResponseEntity deleteDispositif(@PathVariable(value = "id") Long id) {
		dispositifService.delete(id);
		return ResponseEntity.noContent().build();
	}

	@GetMapping(value = "/filters", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Récupère tous les filtres possibles pour les dispositifs")
	public ResponseEntity<List<DispositifFilterDTO>> getAvailableFilters() {
		return ResponseEntity.ok(dispositifService.getAvailableFilters());
	}

	@PutMapping(value = "/{id}/remove")
	@ApiOperation(value = "Permet de déclarer un dispositif comme 'déposé'")
	public ResponseEntity removeDispositif(@PathVariable("id") Long id) {
		DispositifDTO dispositifDTO = dispositifService.removeDispositif(id);
		return ResponseEntity.ok(dispositifDTO);
	}
}
