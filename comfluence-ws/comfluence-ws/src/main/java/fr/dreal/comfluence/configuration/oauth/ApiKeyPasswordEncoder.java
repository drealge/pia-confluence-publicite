package fr.dreal.comfluence.configuration.oauth;

import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @author SopraGroup
 */
public class ApiKeyPasswordEncoder implements PasswordEncoder {
    @Override
    public String encode(CharSequence charSequence) {
        if(charSequence == null) {
            return "";
        }
        return charSequence.toString(); //no encoding for apikey
    }

    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        if(rawPassword == null || encodedPassword == null) {
            return false;
        }
        return rawPassword.equals(encodedPassword);
    }
}
