package fr.dreal.comfluence.configuration.oauth;

import fr.dreal.comfluence.business.dto.UserDto;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;
import java.util.Objects;

public class ConnectedUserDetails extends User {
	@Getter @Setter
	private transient UserDto user;
	
	public ConnectedUserDetails(UserDto user, String pwd, Collection<? extends GrantedAuthority> authorities) {
		super(user.getMail(), pwd, authorities);
		this.user = user;
	}
	
	@Override
	public boolean isAccountNonExpired() {
		return true;
	}
	
	@Override
	public boolean isAccountNonLocked() {
		return true;
	}
	
	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		if (!super.equals(o))
			return false;
		ConnectedUserDetails that = (ConnectedUserDetails) o;
		return user.equals(that.user);
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), user);
	}
}
