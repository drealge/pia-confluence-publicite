package fr.dreal.comfluence.ws;

import fr.dreal.comfluence.business.dto.RoleDto;
import fr.dreal.comfluence.service.RoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author SopraGroup
 */
@RestController
@Controller
@RequestMapping("/role")
@Api(value = "Api pour interragir avec les rôles")
public class RoleController {
    private final RoleService roleService;

    public RoleController(RoleService roleService) {
        this.roleService = roleService;
    }

    @ApiOperation(value = "Permet de récupérer la liste des roles", response = List.class)
    @GetMapping("")
    public ResponseEntity<List<RoleDto>> getAllRoles() {
        List<RoleDto> result = roleService.findAll();
        return ResponseEntity.ok(result);
    }
}
