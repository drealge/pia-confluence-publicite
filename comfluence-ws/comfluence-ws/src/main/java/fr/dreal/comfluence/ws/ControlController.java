package fr.dreal.comfluence.ws;

import fr.dreal.comfluence.business.dto.ControlDto;
import fr.dreal.comfluence.service.ControlService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;


@RestController
@RequestMapping("/control")
@Api(value = "Api pour interragir avec les contrôles")
public class ControlController {
	
	private final ControlService controlService;
	
	public ControlController(ControlService controlService) {
		this.controlService = controlService;
	}
	
	@ApiOperation(value = "Permet de créer un contrôle", response = ControlDto.class)
	@PostMapping("")
	public ResponseEntity<ControlDto> save(@RequestBody ControlDto controlDto) {
		ControlDto result = controlService.save(controlDto);
		if (result != null) {
			return ResponseEntity.ok(result);
		} else {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@ApiOperation(value = "Permet de récupérer un contrôle", response = ControlDto.class)
	@GetMapping("/{id}")
	public ResponseEntity<ControlDto> findById(@PathVariable Long id) {
		ControlDto result = controlService.findById(id);
		if (result != null) {
			return ResponseEntity.ok(result);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@ApiOperation(value = "Permet de récupérer le contrôle du jour associé à un controlleur", response = ControlDto.class)
	@GetMapping("/active/byControllerMail/{mail}")
	public ResponseEntity<ControlDto> findActiveByControllerMail(@PathVariable String mail) {
		ControlDto result = controlService.findActiveByUserMail(mail);
		if (result != null) {
			return ResponseEntity.ok(result);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@ApiOperation(value = "Permet de supprimer un contrôle", response = ControlDto.class)
	@DeleteMapping("/{id}")
	public ResponseEntity<ControlDto> deleteById(@PathVariable Long id) {
		ControlDto result = controlService.findById(id);
		if (result != null) {
			controlService.delete(id);
			return ResponseEntity.ok(result);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@ApiOperation(value = "Permet de récupérer les contrôles associés à un département", response = ControlDto.class)
	@GetMapping("/byDepartment/{departmentCode}")
	public ResponseEntity<List<ControlDto>> findByDepartmentCode(@PathVariable Long departmentCode,
			@RequestParam(required = false) Integer countFetch, @RequestParam(required = false) Date beforeDate) {
		List<ControlDto> result = controlService.findByDepartmentCode(departmentCode, beforeDate, countFetch);
		return ResponseEntity.ok(result);
	}
}
