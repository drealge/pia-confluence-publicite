package fr.dreal.comfluence.ws;

import fr.dreal.comfluence.business.dto.dispositif.StatisticsDispositifsDTO;
import fr.dreal.comfluence.business.dto.dispositif.StatisticsProceduresDTO;
import fr.dreal.comfluence.service.StatisticsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author SopraGroup
 */
@RestController
@RequestMapping("/statistics")
@Api(value = "Récupération des statistiques de comfluence")
public class StatisticsController {
    private final StatisticsService statisticsService;

    public StatisticsController(StatisticsService statisticsService) {
        this.statisticsService = statisticsService;
    }

    @ApiOperation(value = "Retourne les statistiques sur les dispositifs pour un département donné", response = StatisticsDispositifsDTO.class)
    @GetMapping("/{codeDep}/dispositifs")
    public ResponseEntity<StatisticsDispositifsDTO> findDispostififsStatistiques(@PathVariable("codeDep")Long codeDep) {
        StatisticsDispositifsDTO dto = statisticsService.findDispositifsStatistiques(codeDep);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @ApiOperation(value = "Retourne les statistiques sur les procédures", response = StatisticsDispositifsDTO.class)
    @GetMapping("/{codeDep}/procedures")
    public ResponseEntity<List<StatisticsProceduresDTO>> findProceduresStatistiques(@PathVariable("codeDep")Long codeDep) {
        List<StatisticsProceduresDTO> dtoList = statisticsService.findProcedureStatistiques(codeDep);
        return new ResponseEntity<>(dtoList, HttpStatus.OK);
    }
}
