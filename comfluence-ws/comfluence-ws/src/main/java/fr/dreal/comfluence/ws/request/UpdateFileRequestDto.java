package fr.dreal.comfluence.ws.request;

import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author SopraGroup
 */
public class UpdateFileRequestDto {
    @Getter @Setter
    private MultipartFile file;
}
