package fr.dreal.comfluence.configuration.oauth;

import fr.dreal.comfluence.service.RoleService;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler;

@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

    /**
     * Define URL patterns to enable OAuth2 security
     *
     * @param http
     * @throws Exception
     */
    @Override
    public void configure(HttpSecurity http) throws Exception {
        // @formatter:off
		http
				.cors(Customizer.withDefaults())
				.authorizeRequests(authorizeRequests ->
						authorizeRequests
								.antMatchers("/version").permitAll()
                .antMatchers("/layer/proxy/**").permitAll()
								.antMatchers("/user**").access("hasRole('"+ RoleService.ROLE_ADMIN +"')")
								.antMatchers("/role**").access("hasRole('"+ RoleService.ROLE_ADMIN +"')")
								.antMatchers("/**").access("hasRole('"+ RoleService.ROLE_USER +"')"))
				.exceptionHandling(exceptionHandling ->
						exceptionHandling.accessDeniedHandler(new OAuth2AccessDeniedHandler()));
		// @formatter:on
    }
}
