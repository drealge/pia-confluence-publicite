package fr.dreal.comfluence.externalapi;

import fr.dreal.comfluence.externalapi.dto.WFSRequest;
import fr.dreal.comfluence.externalapi.ws.impl.LayerWsImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author SopraGroup
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = LayerWsImpl.class)
public class LayerWsTests {
    @Autowired
    private transient LayerWsImpl layerWsImpl;

    @Test
    public void testFindRestriction() {
        WFSRequest wfsRequest = new WFSRequest();
        wfsRequest.setMap("/home/atlas-mapserver/production/var/data/MD_3806/MD_3806.map");
        wfsRequest.setRequest("GetFeature");
        wfsRequest.setService("WFS");
        wfsRequest.setUrl("http://atlas.patrimoines.culture.fr/cgi-bin/mapserv");
        wfsRequest.setVersion("1.1.0");

        //        String url = "http://atlas.patrimoines.culture.fr/cgi-bin/mapserv?map=/home/atlas-mapserver/production/var/data/MD_3806/MD_3806.map";
//        Map<String, String> params = new HashMap<>();
//        params.put("service", "WFS");
//        params.put("version", "1.1.0");
//        params.put("REQUEST", "GetFeature");
//        params.put("typeName", "MD_3806");
//        params.put("propertyName", "categorie");
//        params.put("map", "/home/atlas-mapserver/production/var/data/MD_3806/MD_3806.map");

//        layerWsImpl.findRestriction(new Coordinate(832961.17, 6784408.77), wfsRequest, "MD_3806","categorie" );
    }
}
