package fr.dreal.comfluence.externalapi.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author SopraGroup
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter @Setter
public class GeoApiCommune {
    /** Nom de la commune **/
    private String nom;
    /** code (INSEE) de la commune **/
    private String code;
    /** Code du département associé **/
    private Long codeDepartement;
    /** Code de la région associée **/
    private Long codeRegion;
    /** Population **/
    private String population;
    /** Liste des codes postaux de la commune **/
    private List<String> codesPostaux;

    public static final String NOM_FIELD = "nom";
    public static final String NOM_CODE = "code";
    public static final String NOM_CODE_DEP = "codeDepartement";
    public static final String NOM_CODE_REG = "codeRegion";
    public static final String NOM_POP = "population";
    public static final String NOM_CODEPOSTAUX = "codesPostaux";
}
