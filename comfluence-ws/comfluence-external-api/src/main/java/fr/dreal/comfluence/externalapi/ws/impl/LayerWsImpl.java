package fr.dreal.comfluence.externalapi.ws.impl;

import fr.dreal.comfluence.externalapi.dto.WFSRequest;
import fr.dreal.comfluence.externalapi.dto.WXSResponse;
import fr.dreal.comfluence.externalapi.ws.LayerWs;
import org.apache.commons.io.IOUtils;
import org.geotools.factory.CommonFactoryFinder;
import org.geotools.filter.v1_1.OGC;
import org.geotools.filter.v1_1.OGCConfiguration;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.geotools.util.factory.GeoTools;
import org.geotools.xsd.Configuration;
import org.geotools.xsd.Encoder;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.Point;
import org.opengis.filter.Filter;
import org.opengis.filter.FilterFactory2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author SopraGroup
 */
@Service
public class LayerWsImpl implements LayerWs {
    private static final Logger LOGGER = LoggerFactory.getLogger(LayerWsImpl.class);

    @Override
    public WXSResponse proxyLayer(String url, Map<String, String> queryMap) {
        Client client = ClientBuilder.newClient();
        WebTarget webTarget = client.target(url);
        if (queryMap != null) {
            for (Map.Entry<String, String> stringStringEntry : queryMap.entrySet()) {
                webTarget = webTarget.queryParam(stringStringEntry.getKey(), stringStringEntry.getValue());
            }
        }
        Response response = webTarget.request().get();
        WXSResponse result = null;
        if (response.getStatus() == HttpStatus.OK.value()) {
            InputStream in = response.readEntity(InputStream.class);
            try {
                byte[] content = new byte[in.available()];
                IOUtils.read(in, content);
                result = new WXSResponse();
                result.setContent(content);
                result.setContentType(response.getMediaType());
            } catch (IOException e) {
                LOGGER.error(e.getMessage(), e);
            }

        }
        response.close();
        return result;
    }

    public List<String> findRestriction(Coordinate coordinate, WFSRequest wfsRequest, String typeName,
            String... propertyNameList) {
        Set<String> result = new HashSet<>();
        Map<String, String> params = new HashMap<>();
        params.put("service", wfsRequest.getService());
        params.put("version", wfsRequest.getVersion());
        params.put("REQUEST", wfsRequest.getRequest());
        params.put("typeName", typeName);
        if (propertyNameList != null) {
            params.put("propertyName", Arrays.stream(propertyNameList).collect(Collectors.joining(",")));
        }
        params.put("map", wfsRequest.getMap());

        GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory(null);
        Point point = geometryFactory.createPoint(coordinate);
        FilterFactory2 ff = CommonFactoryFinder.getFilterFactory2(GeoTools.getDefaultHints());
        Filter filter = ff.contains(ff.property("msGeometry"), ff.literal(point));
        Configuration configuration = new OGCConfiguration();
        Encoder encoder = new Encoder(configuration);
        try {
            params.put("filter", encoder.encodeAsString(filter, OGC.Filter));
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }

        Client client = ClientBuilder.newClient();
        WebTarget webTarget = client.target(wfsRequest.getUrl());
        for (Map.Entry<String, String> paramEntry : params.entrySet()) {
            webTarget = webTarget.queryParam(paramEntry.getKey(), paramEntry.getValue());
        }
        Response response = webTarget.request().get();
        if (response.getStatus() == HttpStatus.OK.value()) {
            String contentType = (String) response.getHeaders().getFirst("Content-Type");
            if (contentType.startsWith("text/xml")) {
                response.getHeaders().putSingle("Content-Type", MediaType.TEXT_XML);
            }
            byte[] content = response.readEntity(byte[].class);
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            Document dom = null;
            try {
                DocumentBuilder db = dbf.newDocumentBuilder();
                dom = db.parse(new ByteArrayInputStream(content));
            } catch (IOException | SAXException | ParserConfigurationException e) {
                LOGGER.error("Pour le type name : " + typeName + " et les coordonnées : " + coordinate.toString(), e);
            }
            if(dom != null) {
                for (String property : propertyNameList) {
                    result.addAll(extractFromResponse(dom, wfsRequest.getNameSpace() + ':' + property));
                }
            }
        }
        response.close();
        return new ArrayList<>(result);
    }

    private List<String> extractFromResponse(Document dom, String name) {
        List<String> result = new ArrayList<>();
        NodeList nodes = dom.getElementsByTagName(name);
        for (int index = 0; index < nodes.getLength(); index++) {
            Element searchedNode = (Element) nodes.item(index);
            result.add(searchedNode.getTextContent());
        }
        return result;
    }
}
