package fr.dreal.comfluence.externalapi.dto;

import javax.ws.rs.core.MediaType;
import lombok.Getter;
import lombok.Setter;

/**
 * @author SopraGroup
 */
@Getter @Setter
public class WXSResponse {
    private byte[] content;
    private MediaType contentType;
}
