package fr.dreal.comfluence.externalapi.ws;

import fr.dreal.comfluence.externalapi.dto.GeoApiCommune;

import java.util.List;

/**
 * cf: https://geo.api.gouv.fr/decoupage-administratif/communes
 * @author SopraGroup
 */
public interface GeoApiCommunesWs {
    /**
     * Recherche les communes en fonction d'un certain nombre de critères.
     * @param codePostal
     * @return Liste de {@link GeoApiCommune}
     */
    List<GeoApiCommune> searchCommunes(String codePostal);

    /**
     * Recherche une commune avec son code insee
     * @param codeInsee code insee
     * @return {@link GeoApiCommune}
     */
    GeoApiCommune commune(String codeInsee, List<String> fields);
}
